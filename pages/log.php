﻿<?php
   session_start();
   if (!isset($_SESSION['name']))
   {
       header('Location: ./index.php');
   }
$id = $_SESSION['id'];
require("../config.php");
?>
<!DOCTYPE html>
<head>
   <meta charset="utf-8" />
   <link rel="icon" href="assets/images/icon.JPG">
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <!-- BOOTSTRAP STYLES-->
   <link href="assets/css/bootstrap.css" rel="stylesheet" />
   <!-- FONTAWESOME STYLES-->
   <link href="assets/css/font-awesome.css" rel="stylesheet" />
   <!-- CUSTOM STYLES-->
   <link href="assets/css/custom.css" rel="stylesheet" />
   <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   <!-- chosen dropdown -->
   <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css">
   <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
   <script type="text/javascript">
      $(function() 
      {
        $(".chzn-select").chosen();
      });
   </script>
   <!-- chosen dropdown -->
   <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>
   <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
   <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
   <style type="text/css">
      .chosen-container-single .chosen-single 
      {
        position: relative;
        display: block;
        overflow: hidden;
        padding: 0 0 0 8px;
        height: 33px;
        border: 1px solid #aaa;
        border-radius: 0px;
        background-color: #fff;
        background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(20%, #ffffff), color-stop(50%, #f6f6f6), color-stop(52%, #eeeeee), color-stop(100%, #f4f4f4));
        background: -webkit-linear-gradient(top, #ffffff 20%, #f6f6f6 50%, #eeeeee 52%, #f4f4f4 100%);
        background: -moz-linear-gradient(top, #ffffff 20%, #f6f6f6 50%, #eeeeee 52%, #f4f4f4 100%);
        background: -o-linear-gradient(top, #ffffff 20%, #f6f6f6 50%, #eeeeee 52%, #f4f4f4 100%);
        background: linear-gradient(top, #ffffff 20%, #f6f6f6 50%, #eeeeee 52%, #f4f4f4 100%);
        background-clip: padding-box;
        box-shadow: 0 0 3px white inset, 0 1px 1px rgba(0, 0, 0, 0.1);
        color: #444;
        text-decoration: none;
        white-space: nowrap;
        line-height: 30px;
      }
   </style>
</head>
<body>
   <div id="wrapper">
      <?php include('includes/menu.php'); ?>
      <div id="page-wrapper" >
         <div id="page-inner">
            <div class="row">
               
               <!-- Advanced Tables -->
               <div class="panel panel-default" style="margin-top: -20px">
                  <div class="panel-heading">
                     <div class="row">
                        <div class="col-lg-12">
                           
                        </div>
                     </div>
                     <div class="row">
                           <div class="col-lg-3">
                              <div class="col-lg-11">
                                 <p><label>User Name</label></p>
                                 <select class="chzn-select form-control" name="ftype" id="ftype">
                                    <option value="">--select--</option>
                                    
                                    <?php
                                       $sql = "select * from user where usts!='L' ";
                                       $result = mysql_query($sql,$conn);
                                       if (mysql_num_rows($result) > 0)
                                       {
                                          while ($row = mysql_fetch_assoc($result)) 
                                          {
                                                
                                                if (isset($_REQUEST['user']) && $_REQUEST['user']==$row['userid']) 
                                                {
                                                  echo '<option value="'.$row['userid'].'" selected>'.$row['ufullname'].'</option>';
                                                }
                                                else
                                                {
                                                  echo '<option value="'.$row['userid'].'">'.$row['ufullname'].'</option>';
                                                }
                                          } 
                                       }
                                    ?>
                                    
                                 </select>
                              </div>
                           </div>
                           
                           <div class="col-lg-3">
                              <p><label>From Date</label></p>
                              <div class="input-group">
                                 <input autocomplete="off" class="form-control" id="from_date" name="from_date" placeholder="MM/DD/YYYY" type="text" value="<?php if(isset($_REQUEST['from_date']) && $_REQUEST['from_date'] !=''){echo date("d-m-Y",strtotime($_REQUEST['from_date']));}else{echo date("d-m-Y");} ?>" />
                              </div>
                           </div>

                           <div class="col-lg-3">
                              <p><label>To Date</label></p>
                              <div class="input-group">
                                 <input autocomplete="off" class="form-control col-lg-6" id="to_date" name="to_date" placeholder="MM/DD/YYYY" type="text" value="<?php if(isset($_REQUEST['to_date']) && $_REQUEST['to_date']!=''){echo date("d-m-Y",strtotime($_REQUEST['to_date']));}else{echo date("d-m-Y");}?>" />
                              </div>
                           </div>

                           <script type="text/javascript">
                             function search()
                             { 
                              var x="log.php?user="+document.getElementById("ftype").value+"&from_date="+document.getElementById("from_date").value+"&to_date="+document.getElementById("to_date").value;
                              window.location=x;
                             }
                           </script>

                           <div class="col-lg-offset-8 col-lg-4">
                              <br>
                              <!-- <input type="submit" name="search" class="btn btn-primary" value="Search" onclick="search();"> -->
                              <button type="submit" name="search" class="btn btn-primary" value="Search" onclick="search();"><i class="fa fa-search"></i> Search</button>
                              <a class="btn btn-danger" href="log.php"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a>
                              <a class="btn btn-warning" href="index.php"><i class="fa fa-arrow-left"></i> Back</a>
                           </div>
                     </div>
                  </div>
                  <div class="panel-body">
                     <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered table-condensed">
                           <thead>
                              <tr>
                                 <th>User</th>
                                 <th>Device Type</th>
                                 <th>Ip</th>
                                 <th>Action</th>
                                 <th>Date Time</th>
                              </tr>
                           </thead>
                           <tbody>
                                
                            <?php
                            
                            if(isset($_REQUEST['user']) && $_REQUEST['user']>0 && $_REQUEST['from_date']!='' && $_REQUEST['to_date']!='')
                            {
                                $uid = $_REQUEST['user'];
                                $from_date = date('Y-m-d', strtotime($_REQUEST['from_date']));
                                $to_date = date('Y-m-d', strtotime($_REQUEST['to_date']));
                                $sql = "select * from log where user_id = $uid and date(date_time) between '$from_date' and '$to_date' ORDER BY log_id DESC";
                                $result = mysql_query($sql,$conn);
                            }
                            else
                            {
                                $sql = "SELECT * FROM `log` ORDER BY `log`.`log_id` DESC";
                                $result = mysql_query($sql,$conn);
                            }
                            

                            if (mysql_num_rows($result) > 0)
                            {
                                while ($row = mysql_fetch_assoc($result)) 
                                {
                                    $sql2 = "select ufullname from user where userid = ".$row['user_id'];
                                    $result2 = mysql_query($sql2,$conn);
                                    $r=mysql_fetch_assoc($result2);
                                    echo "<tr>";
                                    echo "<td>".$r['ufullname']."</td>";
                                    echo "<td>".$row['device_type']."</td>";
                                    echo "<td>".$row['ip_address']."</td>";
                                    echo "<td>".$row['action']."</td>";
                                    echo "<td>".$row['date_time']."</td>";
                                    echo "</tr>";
                                } 
                            }
                            ?>
                                
                           </tbody>
                           <tfoot>
                              <tr>
                                 <th>User</th>
                                 <th>Device Type</th>
                                 <th>Ip</th>
                                 <th>Action</th>
                                 <th>Date Time</th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
               <!--End Advanced Tables -->
            </div>
            <!-- /. ROW  -->
            <hr />
         </div>
         <!-- /. PAGE INNER  -->
      </div>
      <!-- /. PAGE WRAPPER  -->
   </div>
   <!-- /. WRAPPER  -->
   <!-- BOOTSTRAP SCRIPTS -->
   <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
   <script src="assets/js/bootstrap.min.js"></script>
   <!-- CUSTOM SCRIPTS -->
   <script src="assets/js/custom.js"></script>
   <!-- Include Date Range Picker -->
   
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
   <script>
      $(document).ready(function(){
      var from_date_input=$('input[name="from_date"]'); //our date input has the name "date"
      var to_date_input=$('input[name="to_date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      from_date_input.datepicker({
      format: 'dd-mm-yyyy',
      container: container,
      todayHighlight: true,
      autoclose: true,
      })
      to_date_input.datepicker({
      format: 'dd-mm-yyyy',
      container: container,
      todayHighlight: true,
      autoclose: true,
      })
      })
   </script>
   <!-- chosen script -->
   <script src="assets/js/chosen.jquery.js" type="text/javascript"></script>
   <script src="assets/js/init.js" type="text/javascript" charset="utf-8"></script>
   <!-- chosen script -->

   
   <!-- DATA TABLE SCRIPTS -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
	<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
   <script>
	$( document ).ready(function() {
	$('#example').DataTable({
	"processing": true,
	"dom": 'lBfrtip',
	"buttons": [
	{
	extend: 'collection',
	text: 'Export',
	buttons: [
	'copy',
	'excel',
	'csv',
	'pdf',
	'print'
	]
	}
	]
	});
	});
   </script>
   <!-- DATA TABLE SCRIPTS -->

  <!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css"/>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.example-getting-started').multiselect({
                includeSelectAllOption: true,
                onChange: function(option, checked) 
                {
                    // alert('Not triggered when clicking the select all!');
                },
            });
        });
    </script>
</body>
</html>