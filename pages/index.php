﻿<?php
    session_start();
    if(!isset($_SESSION['name']))
    {
        header('Location: ../index.php');
    }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="icon" href="assets/images/icon.JPG">
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <!-- BOOTSTRAP STYLES-->
  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="assets/css/custom.css" rel="stylesheet" />
  <!-- GOOGLE FONTS-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
<?php
  if ($_SESSION['utype']=='A') 
  { ?>
        <div id="wrapper">
      <?php include('includes/menu.php'); ?>
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">           
      <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-brown set-icon">
                    <i class="fa fa-users"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">
                      <?php 
                          require("../config.php");
                          $sql = "SELECT COUNT(*) FROM user where (usts = 'A' || usts = 'D') ";
                          $result = mysql_query($sql,$conn);
                          $row = mysql_fetch_array($result);
                          $total = $row[0];
                          echo $total;
                      ?>
                    </p>
                    <p class="text-muted">Total Users</p>
                </div>
             </div>
         </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">           
      <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa-user"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">
                      <?php 
                          require("../config.php");
                          $sql = "SELECT COUNT(*) FROM user WHERE usts = 'A' ";
                          $result = mysql_query($sql,$conn);
                          $row = mysql_fetch_array($result);
                          $total = $row[0];
                          echo $total;
                      ?>
                    </p>
                    <p class="text-muted">Active Users</p>
                </div>
             </div>
         </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">           
      <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-user"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">
                        <?php 
                          require("../config.php");
                          $sql = "SELECT COUNT(*) FROM user WHERE usts = 'D' ";
                          $result = mysql_query($sql,$conn);
                          $row = mysql_fetch_array($result);
                          $total = $row[0];
                          echo $total;
                      ?>
                     </p>
                    <p class="text-muted">Deact Users</p>
                </div>
             </div>
         </div>

         <div class="col-md-3 col-sm-6 col-xs-6">           
      <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-blue set-icon">
                    <i class="fa fa-lock"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">
                        <?php 
                          require("../config.php");
                          $sql = "SELECT COUNT(*) FROM user WHERE utype = 'A' ";
                          $result = mysql_query($sql,$conn);
                          $row = mysql_fetch_array($result);
                          $total = $row[0];
                          echo $total;
                      ?>
                     </p>
                    <p class="text-muted">Admins</p>
                </div>
             </div>
         </div>

         <div class="col-md-3 col-sm-6 col-xs-6">           
      <div class="panel panel-back noti-box">
                <span class="icon-box set-icon" style="background-color: #70C5DA; color: #fff;">
                    <i class="fa fa-file"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">
                      <?php 
                          require("../config.php");
                          $sql = "SELECT COUNT(*) FROM user_subfile ";
                          $result = mysql_query($sql,$conn);
                          $row = mysql_fetch_array($result);
                          $total = $row[0];
                          echo $total;
                      ?>
                    </p>
                    <p class="text-muted">Total Files</p>
                </div>
             </div>
         </div>


      </div><!-- /. ROW  -->                              
    </div><!-- /. PAGE INNER  -->
  </div><!-- /. PAGE WRAPPER  -->
</div><!-- /. WRAPPER  -->
<?php  }
else
{ ?>
        <div id="wrapper">
      <?php include('includes/menu.php'); ?>
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">

                <div class="col-md-3 col-sm-6 col-xs-6">           
      <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-brown set-icon">
                    <i class="fa fa-file"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">
                      <?php 
                          require("../config.php");
                          $sql = "SELECT count(*) as total FROM user_file INNER JOIN user_subfile on `user_file`.`fileid` = `user_subfile`.`fileid` AND `user_file`.`crby` = ".$_SESSION['id'];
                          $result = mysql_query($sql,$conn);
                          $row = mysql_fetch_array($result);
                          $total = $row['total'];
                          echo $total;
                      ?>
                    </p>
                    <p class="text-muted">Total Files</p>
                </div>
             </div>
         </div>

      </div><!-- /. ROW  -->                              
    </div><!-- /. PAGE INNER  -->
  </div><!-- /. PAGE WRAPPER  -->
</div><!-- /. WRAPPER  -->
<?php }
?>


    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
