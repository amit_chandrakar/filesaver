<?php
    session_start();
    $id = $_GET['id'];
    if (!isset($_SESSION['name']))
    {
        header('Location: ./index.php');
    }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <link rel="icon" href="assets/images/icon.JPG">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <?php
			include('includes/menu.php'); 
			require("../config.php");
			$sql = "SELECT * FROM user where userid = '$id' ";
			$result = mysql_query($sql,$conn);
			$row = mysql_fetch_assoc($result);
		?>
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
				
					<div class="col-md-12">
						<a href="manage_user.php" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Back</a>
						<h2 class="text-center">View User Record</h2>
						<hr>	
					</div>

                    <div class="col-md-3 text-center"><label>User Id</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['userid'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>Full Name</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['ufullname'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>User Name</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['uname'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>Password</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo base64_encode($row['upwd']);?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>Contact</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['ucontact'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>Email</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['uemail'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>User Type</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['utype'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>Execute Permission</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['uexecute'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>Add Permission</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['uadd'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>View Permission</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['uview'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>Delete Permission</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['udelete']; ?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>Created By</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">

                          <?php
                            $sql1 = "SELECT * FROM user where userid=".$row['crby'];
                            $result1 = mysql_query($sql1,$conn);
                            $row1 = mysql_fetch_assoc($result1);
                          ?>
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row1['ufullname'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>Created Date</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo date('d-m-Y', strtotime($row['crdate']));?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-center"><label>Reporting</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <?php
                            $uid = $row['reporting'];
                             if($uid == 0) 
                             { ?>
                              <input type="text" class="form-control disabled" readonly value="None">
                             <?php }
                             else
                             {
                               require("../config.php");
                               $sql = "SELECT ufullname FROM user where userid = $uid";
                               $result = mysql_query($sql,$conn);
                               $row = mysql_fetch_assoc($result);
                               
                               ?>
                               <input type="text" class="form-control disabled" readonly value="<?php echo $row['ufullname']; ?>">
                             <?php } 
                           ?>
                        </div>
                      </div>
    	     		
        	</div>
			</div><!-- /. page-inner  -->
    	</div><!-- /. page-wrapper  -->
    </div><!-- /.wrapper  -->


    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
   
</body>
</html>
