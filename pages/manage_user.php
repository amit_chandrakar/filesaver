﻿<?php
    session_start();
    if (!isset($_SESSION['name']) || $_SESSION['utype']!='A')
    {
        header('Location: logout.php');
    }    
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
      <link rel="icon" href="assets/images/icon.JPG">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin Panel</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
   
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <?php include('includes/menu.php'); ?>
        <div id="page-wrapper" >
            <div id="page-inner" style="margin-top:-40px">
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (isset($_GET['result']) && $_GET['result'] == "success")
                    {
                        echo "
                        <div class='alert alert-success alert-dismissible fade in'>
                        <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        <strong>Success!</strong> User Deleted.
                        </div>
                        ";
                    }
                    else if(isset($_GET['result']) && $_GET['result'] == "fail")
                    {
                        echo "
                        <div class='alert alert-danger alert-dismissible fade in'>
                        <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        <strong>Error..! User Not Deleted.</strong>
                        </div>
                        ";
                    }
                    else if (isset($_GET['update']) && $_GET['update'] == "success")
                    {
                            echo "
                            <div class='alert alert-success alert-dismissible fade in'>
                            <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>Success!</strong> Data Updated.
                            </div>
                            ";
                    }
                    else if(isset($_GET['update']) && $_GET['update'] == "fail")
                    {
                        echo "
                            <div class='alert alert-danger alert-dismissible fade in'>
                            <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>Success!</strong> Data Updated.
                            </div>
                            ";
                    }
                    else if(isset($_GET['insert']) && $_GET['insert'] == "success")
                    {
                        echo "
                            <div class='alert alert-success alert-dismissible fade in'>
                            <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>Success!</strong> User and Folder Created Successfully.
                            </div>
                            ";
                    }
                    else if(isset($_GET['insert']) && $_GET['insert'] == "fail")
                    {
                        echo "
                            <div class='alert alert-danger alert-dismissible fade in'>
                            <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>Fail!</strong> User Not Created.
                            </div>
                            ";
                    }
                    ?>

                    <!-- Advanced Tables -->
                    <div class="panel panel-default" style="margin-bottom: 0px;">
                        <div class="panel-heading">
                            <a href="insert.php" class="btn btn-primary text-right"><i class="fa fa-plus"></i> Add New user</a>
                            <a href="manage_user.php" class="btn btn-primary text-right"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>
                            <center> <span class="h3" style="color: #428bca">Manage Users</span></center>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-condensed" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>UserName</th>
                                            <!-- <th>Password</th> -->
                                            <th>User Type</th>
                                            <th>User Status</th>
                                            <th>Contact</th>
                                            <!-- <th>Permissions</th> -->
                                            <!-- <th>Created By</th> -->
                                            <th>Created Date</th>
                                            <th>Reporting</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                            
                                            <?php 
                                        require("../config.php");
                                        $id =$_SESSION['id'];
                                        $sql = "SELECT * FROM user where usts!='L' AND userid!=$id ";
                                        $result = mysql_query($sql,$conn);
                                        if (mysql_num_rows($result) > 0)
                                        {
                                            
                                            while ($row = mysql_fetch_assoc($result)) 
                                            {
                                                $id = $row['userid'];
                                                $reporting = $row['reporting'];
                                                echo "<tr>";?>
                                                <td>
                                                    <a data-toggle="tooltip" title="Edit User Detail" href="edit.php?id=<?= $id ?>&reporting=<?= $reporting ?>"><img src="assets/img/edit.png"></a>
                                                    <a data-toggle="tooltip" title="View Detail" href="view.php?id=<?= $id ?>"><img src='assets/img/view.png'></a>
                                                    <a onclick="return confirm('Are you sure?')" data-toggle="tooltip" title="Delete User" href="delete.php?id=<?= $id ?>"><img src='assets/img/delete.png'></a>
                                                </td>

                                                <?php
                                                echo "<td>".$row['userid']."</td>";
                                                echo "<td>".$row['ufullname']."</td>";
                                                echo "<td>".$row['uname']."</td>";
                                                // echo "<td>".base64_decode($row['upwd'])."</td>";
                                                if ($row['utype'] == "A") 
                                                {
                                                    echo "<td>"."Admin"."</td>";
                                                }
                                                else
                                                {
                                                    echo "<td>"."User"."</td>";
                                                }

                                                if ($row['usts'] == "A") 
                                                {
                                                    echo "<td>"."Active"."</td>";
                                                }
                                                else
                                                {
                                                    echo "<td>"."Deactive"."</td>";
                                                }
                                                echo "<td>".$row['ucontact']."</td>";
                                                // echo "<td>".$row['uexecute'].$row['uadd'].$row['udelete'].$row['uview']."</td>";
                                                // echo "<td>".$row['crby']."</td>";
                                                echo "<td>".date('d-m-Y', strtotime($row['crdate']))."</td>";

                                                $sql1 = "SELECT * FROM user where userid=".$row['reporting'];
                                                $result1 = mysql_query($sql1,$conn);
                                                $row1 = mysql_fetch_assoc($result1);
                                                if ($row1['ufullname']=="")
                                                {
                                                    echo "<td>None</td>";
                                                }
                                                else
                                                {
                                                    echo "<td>".$row1['ufullname']."</td>";
                                                }
                                                
                                                echo "</tr>";
                                            }
                                        }
                                    ?>

                                        
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Action</th>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>UserName</th>
                                            <!-- <th>Password</th> -->
                                            <th>User Type</th>
                                            <th>User Status</th>
                                            <th>Contact</th>
                                            <!-- <th>Permissions</th> -->
                                            <!-- <th>Created By</th> -->
                                            <th>Created Date</th>
                                            <th>Reporting</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>

        
  
                <!-- /. ROW  -->
        </div>
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function()
        {
            $('[data-toggle="tooltip"]').tooltip(); 
        });
    </script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
        <script>

            $(document).ready(function() 
            {
                $('#dataTables-example').DataTable({
                    "lengthMenu": [[10, 25, 50, 100, -1], [10, 22, 50, 100, "All"]]
                } );
            });

        </script>
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>

   
</body>
</html>
