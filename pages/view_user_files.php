<?php
    session_start();
    
    if (!isset($_SESSION['name']))
    {
        header('Location: ./index.php');
    }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <link rel="icon" href="assets/images/icon.JPG">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <!-- BOOTSTRAP STYLES-->
  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="assets/css/font-awesome.css" rel="stylesheet" />
  <!-- MORRIS CHART STYLES-->

  <!-- GOOGLE FONTS-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
  <!-- TABLE STYLES-->
  <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>

  <div id="wrapper" class="container-fluid">
  <div id="page-wrapper" >
    <div id="page-inner">
      <div class="row">
        <!-- Advanced Tables -->
                    <div class="panel panel-default" style="margin-bottom: 0px;">
                        <div class="panel-heading">
                            <input type="button" class="btn btn-warning" value="Back" onclick="history.go(-1)">
                            <a href="" class="btn btn-danger text-right">Refresh</a>
                            <center> <span class="h3" style="color: #428bca">View Files</span></center>
                            <span><b>User: </b><span><span>
                              <?php
                                require("../config.php");
                                $userid = $_GET['crby'];
                                $sql = "select * from user where userid = $userid"; 
                                $result = mysql_query($sql,$conn);
                                $row = mysql_fetch_assoc($result);
                                echo $row['ufullname'];
                              ?>

                              </span>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-condensed" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>File name</th>
                                            <th>Sub File Name</th>
                                            <th>Uploaded Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                            
                                            <?php 
                                        require("../config.php");
                                        $userid = $_GET['crby'];
                                        $from = $_GET['from'];
                                        $to = $_GET['to'];

                                        $sql = "SELECT `user_subfile`.`order_id`, `user_subfile`.`fup2name`,  `user_subfile`.`f2name` , `user_file`.`crdate` FROM user_file inner join user_subfile on `user_file`.`fileid`=`user_subfile`.`fileid` where `user_file`.`crdate` between '".$from."' AND '".$to."' AND user_file.crby = ".$userid; 
                                        $result = mysql_query($sql,$conn);
                                        if (mysql_num_rows($result) > 0)
                                        {
                                            while ($row = mysql_fetch_assoc($result)) 
                                            {
                                                ?>
                                            <tr>
                                            <td><?php echo $row['order_id'];?></td>
                                            <td><?php echo $row['fup2name'];?></td>
                                            <td><?php echo $row['f2name'];?></td>
                                            <td><?php echo date('d-m-Y', strtotime($row['crdate'])); ?></td>


                                            <td>
                                              <a target="_blank" download href="upload/<?php echo $userid ;?>/<?php echo $row['crdate']; ?>/<?php echo $row['fup2name'];?>">Download</a> |
                                              <a target="_blank" href="upload/<?php echo $userid ;?>/<?php echo $row['crdate']; ?>/<?php echo $row['fup2name'];?>">View</a> 
                                              
                                              <?php 
                                                    if ($_SESSION['uexecute'] == 'Y' || $_SESSION['udelete'] == 'Y') 
                                                    { ?>
                                                      | <a href="delete_file.php?id=<?php echo $row['filesubid']; ?>">Delete</a>
                                                    <?php }
                                                ?>
                                              
                                            </td>
                                            </tr>
                                            <?php 
                                            }
                                        }
                                    ?>   
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Id</th>
                                            <th>File name</th>
                                            <th>Sub File Name</th>
                                            <th>Uploaded Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
      </div>
    </div><!-- /. PAGE INNER  -->    
  </div><!-- /. PAGE   -->
  </div><!-- /. ID=WRAPPER -->


<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="assets/js/dataTables/jquery.dataTables.js"></script>
<script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
<script>
$(document).ready(function () {
$('#dataTables-example').dataTable();
});
</script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
   
</body>
</html>
