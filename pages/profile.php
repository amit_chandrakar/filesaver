<?php
    session_start();

    if (!isset($_SESSION['name']))
    {
        header('Location: ./index.php');
    }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
      <link rel="icon" href="assets/images/icon.JPG">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
   
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <?php include('includes/menu.php'); ?>
        
        <div id="page-wrapper" >
            <div id="page-inner">
              <?php
                if (isset($_GET['update']) && $_GET['update'] == "success")
                    {
                            echo "
                            <div class='alert alert-success alert-dismissible fade in'>
                            <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>Success!</strong> Data Updated.
                            </div>
                            ";
                    }
                    else if(isset($_GET['update']) && $_GET['update'] == "fail")
                    {
                        echo "
                            <div class='alert alert-danger alert-dismissible fade in'>
                            <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>Success!</strong> Data Updated.
                            </div>
                            ";
                    }

              ?>
                <div class="row">
                    
					<div class="col-md-12">
					
					<a href="edit_profile.php" class="btn btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Profile</a>
                    <h2 class="text-center">My Profile</h2><hr>
					</div>
                
				
            		<?php 
  					    require("../config.php");
                $id = $_SESSION['id'];
  					    $sql = "SELECT * FROM user where userid = '$id' ";
  					    $result = mysql_query($sql,$conn);
  				        $row = mysql_fetch_assoc($result);
  				        //echo $row['uname'];
					     ?>

                    <div class="col-md-3 text-left"><label>User Id</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['userid'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>Full Name</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['ufullname'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>User Name</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['uname'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>Password</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo base64_decode($row['upwd']);?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>Contact</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['ucontact'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>Email</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['uemail'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>User Type</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php if($row['utype']=='A'){echo 'Admin';}else{ echo 'User';}?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>Execute Permission</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php if($row['uexecute']=='Y'){echo 'Yes';}else{ echo 'No';}?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>Add Permission</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php if($row['uadd']=='Y'){echo 'Yes';}else{ echo 'No';}?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>View Permission</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php if($row['uview']=='Y'){echo 'Yes';}else{ echo 'No';}?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>Delete Permission</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php if($row['udelete']=='Y'){echo 'Yes';}else{ echo 'No';}?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>Created By</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo $row['crby'];?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>Created Date</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <input type="text" class="form-control disabled" readonly value="<?php echo date('d-m-Y', strtotime($row['crdate']));?>">
                        </div>
                      </div>

                      <div class="col-md-3 text-left"><label>Reporting</label></div>
                      <div class="col-md-3 text-left">
                        <div class="form-group">
                          <?php
                            $uid = $row['reporting'];
                             if($uid == 0) 
                             { ?>
                              <input type="text" class="form-control disabled" readonly value="None">
                             <?php }
                             else
                             {
                               require("../config.php");
                               $sql = "SELECT ufullname FROM user where userid = $uid";
                               $result = mysql_query($sql,$conn);
                               $row = mysql_fetch_assoc($result);
                               
                               ?>
                               <input type="text" class="form-control disabled" readonly value="<?php echo $row['ufullname']; ?>">
                             <?php } 
                           ?>
                        </div>
                      </div>
    	     		
				</div>	
        	</div><!-- /. PAGE INNER  -->    
    	</div><!-- /. page-wrapper  -->
    </div><!-- /. wrapper  -->


    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>

         <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
   
</body>
</html>
