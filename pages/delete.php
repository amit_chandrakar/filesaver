<?php
    session_start();
    $id = $_GET['id'];
    if (!isset($_SESSION['name']))
    {
        header('Location: ./index.php');
    }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
      <link rel="icon" href="assets/images/icon.JPG">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
   
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
<?php include('includes/menu.php'); ?>
        <div id="page-wrapper" >
          <div id="page-inner">
               <?php 
                  require("../config.php");
                  $sql = "UPDATE user set usts='L' where userid = '$id' ";
                  $result = mysql_query($sql,$conn);
                  if ($result) 
                  {
                    $log_query = "select * from user where userid= $id";
                    $log_query_result = mysql_query($log_query,$conn);
                    $log_rs = mysql_fetch_assoc($log_query_result);
                    $user =  $log_rs['ufullname'];
                    $log = "INSERT INTO `log` (`log_id`, `user_id`, `action`, `device_type`, `ip_address`, `date_time`) VALUES (NULL, '".$_SESSION['id']."', 'Success delete user <b>$user</b>', '".$_SERVER['HTTP_USER_AGENT']."', '".$_SERVER['REMOTE_ADDR']."', CURRENT_TIMESTAMP);";
                    $log_result = mysql_query($log,$conn);
                    header('Location: manage_user.php?result=success');
                  }
                  else
                  {
                    $log_query = "select * from user where userid= $id";
                    $log_query_result = mysql_query($log_query,$conn);
                    $log_rs = mysql_fetch_assoc($log_query_result);
                    $user =  $log_rs['ufullname'];
                    $log = "INSERT INTO `log` (`log_id`, `user_id`, `action`, `device_type`, `ip_address`, `date_time`) VALUES (NULL, '".$_SESSION['id']."', 'Fail delete user <b>$user</b>', '".$_SERVER['HTTP_USER_AGENT']."', '".$_SERVER['REMOTE_ADDR']."', CURRENT_TIMESTAMP);";
                    $log_result = mysql_query($log,$conn);
                    header('Location: manage_user.php?result=fail');
                  }
                    //echo $row['uname'];
                ?>
          </div>
        </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
