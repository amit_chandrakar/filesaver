<?php
    session_start();
    
    if (!isset($_SESSION['name']))
    {
        header('Location: ./index.php');
    }
    echo $_REQUEST['ftype'];
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <link rel="icon" href="assets/images/icon.JPG">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <!-- BOOTSTRAP STYLES-->
  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="assets/css/font-awesome.css" rel="stylesheet" />
  <!-- MORRIS CHART STYLES-->

  <!-- CUSTOM STYLES-->
  <link href="assets/css/custom.css" rel="stylesheet" />
  <!-- GOOGLE FONTS-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
  <!-- TABLE STYLES-->
  <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>

  <div id="wrapper">
  <?php include('includes/menu.php'); ?>
  <div id="page-wrapper" >
    <div id="page-inner">
      <div class="row">
        <!-- Advanced Tables -->
                    <div class="panel panel-default" style="margin-top: 0px;">
                        <div class="panel-heading">
                          <?php
                  if (isset($_GET['filedelete']) && $_GET['filedelete'] == "success")
                  {
                    echo "
                    <div class='alert alert-success alert-dismissible fade in'>
                    <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <strong>Success!</strong> File Deleted.
                    </div>
                    ";
                  }
                  else if(isset($_GET['filedelete']) && $_GET['filedelete'] == "fail")
                  {
                    echo "
                    <div class='alert alert-danger alert-dismissible fade in'>
                    <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <strong>File Not Deleted.!</strong>
                    </div>
                    ";
                  }
                  ?>
                            <!-- <input type="button" class="btn btn-warning" value="Back" onclick="history.go(-1)"> -->
                            <button class="btn btn-warning" onclick="history.go(-1)"><i class="fa fa-arrow-left"></i> Back</button>
                            
                            <a href="" class="btn btn-danger text-right"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>
                            <center> <span class="h3" style="color: #428bca">View Files</span></center>
                            <span style="font-size: 18px; text-decoration: blink;">FileName :</span> 
                            <span style="font-size: 18px;"><u> <?php echo $_GET['filename'];  ?></u> </span> 
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-condensed" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>File name</th>
                                            <th>Preview</th>
                                            <th>Sub File Name</th>
                                            <th>Uploaded Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                            
                                            <?php 
                                            $d= $_REQUEST['date'];
                                        require("../config.php");
                                        
                                        $uid = $_SESSION['id'];
                                        $id = $_GET['fileid'];
                                        $date = date('d-m-Y', strtotime($_GET['date']));
                                        $d = date('Y-m-d', strtotime($_GET['date']));
                                        $sql = "SELECT * FROM user_subfile WHERE fileid = $id";
                                        $result = mysql_query($sql,$conn);
                                        if (mysql_num_rows($result) > 0)
                                        {
                                            while ($row = mysql_fetch_assoc($result)) 
                                            {
                                                ?>
                    <tr>
                    <td><?php echo $row['order_id'];?></td>
                    <td><?php echo $row['fup2name'];?></td>
                    <?php
                        $fileid = $_REQUEST['fileid']; 
                          require("../config.php");
                          $sql1 = "SELECT * FROM user_file WHERE fileid=$fileid ";
                          $result1 = mysql_query($sql1,$conn);
                          if (mysql_num_rows($result1) > 0)
                          {
                              $row1 = mysql_fetch_assoc($result1);
                              $crby = $row1['crby'];
                          }
                      ?>
                    <?php if($_REQUEST['user']=='own' ){ ?>
                    <td><img height="80" width="80" src="upload/<?=$uid?>/<?=$d?>/<?=$row['fup2name']?>"></td>
                  <?php }
                  else if($_SESSION['utype']=='A' && $_REQUEST['user']>0)
                  { ?>
                    <td><img height="80" width="80" src="upload/<?php echo $crby; ?>/<?php echo $_REQUEST['date']; ?>/<?php echo $row['fup2name'];?>"></td>
                 <?php }
                 else if($_REQUEST['shared_file']>0)
                 { ?>
                  <td><img height="80" width="80" src="upload/<?php echo $crby; ?>/<?php echo $_REQUEST['date']; ?>/<?php echo $row['fup2name'];?>"></td>
                <?php }

                  ?>
                    <td><?php echo $row['f2name'];?></td>
                    <td><?php echo $date; ?></td>
                    <td>


                      


                      <a target="_blank" download href="upload/<?php echo $crby;?>/<?php echo $_REQUEST['date']; ?>/<?php echo $row['fup2name'];?>">Download</a> |
                      
                      <a target="_blank" href="upload/<?php echo $crby; ?>/<?php echo $_REQUEST['date']; ?>/<?php echo $row['fup2name'];?>">View</a> 
                      
                      <?php 
                            $date1=date_create(date('Y-m-d'));
                            $date2=date_create(date('d-m-Y', strtotime($row1['crdate'])));
                            $diff=date_diff($date1,$date2);
                            $diff->format("%a");

                            if ($diff->format("%a")==0 && ($_SESSION['uexecute'] == 'Y' || $_SESSION['udelete'] == 'Y')) 
                            { ?>
                              | <a href="delete_file.php?id=<?php echo $row['filesubid'];?>&fileid=<?php echo $fileid; ?>&date=<?php echo $_REQUEST['date']; ?>&fileup2name=<?php echo $row['fup2name'];?>">Delete</a>
                            <?php }
                        ?>
                      
                    </td>
                    </tr>
                  <?php 
                                            }
                                        }
                                    ?>

                                        
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Id</th>
                                            <th>File name</th>
                                            <th>Preview</th>
                                            <th>Sub File Name</th>
                                            <th>Uploaded Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
      </div>
    </div><!-- /. PAGE INNER  -->    
  </div><!-- /. PAGE   -->
  </div><!-- /. ID=WRAPPER -->


<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="assets/js/dataTables/jquery.dataTables.js"></script>
<script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
<script>
$(document).ready(function () {
$('#dataTables-example').dataTable();
});
</script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
   
</body>
</html>
