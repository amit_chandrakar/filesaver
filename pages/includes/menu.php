﻿<title>Filesaver</title>
<link rel="icon" href="assets/images/icon.JPG">
<!-- responsive navbar -->
<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0; margin-top: -20px;">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">
        	<?php
        		if ($_SESSION['utype']=='A') 
        		{
        			echo "Admin Panel";
        		}
        		else
        		{
        			echo "User Panel";
        		}
        	?>
    	</a> 
    </div>
    <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
        Welcome, <?php echo $_SESSION['name']; ?> &nbsp; <a href="logout.php" class="btn btn-default square-btn-adjust"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
    </div>
</nav>   
<!-- responsive navbar -->

<!-- /. NAV TOP  -->
<div id="menu">
<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">

            <li class="text-center">
                <img style="display: none;" src="assets/img/find_user2.png" class="user-image"/> 
            </li>

            <li>
                <a href="index.php" class="
                <?php
                    if($_SERVER['PHP_SELF']=='/filesaver/pages/index.php')
                    { 
                        echo 'active-menu';
                    } 
                 ?>
                "><i class="fa fa-dashboard fa-1x"></i> Dashboard</a>
            </li>

            <?php 
                if ($_SESSION['utype'] == "A") 
                { ?>
                    
              
            <li>
                <a href="manage_user.php" class="
                <?php
                    if($_SERVER['PHP_SELF']=='/filesaver/pages/manage_user.php')
                    { 
                        echo 'active-menu';
                    }
                    else if($_SERVER['PHP_SELF']=='/filesaver/pages/insert.php')
                    { 
                        echo 'active-menu';
                    }
                    else if($_SERVER['PHP_SELF']=='/filesaver/pages/edit.php')
                    { 
                        echo 'active-menu';
                    }
                    else if($_SERVER['PHP_SELF']=='/filesaver/pages/view.php')
                    { 
                        echo 'active-menu';
                    } 
                 ?>
                "><i class="fa fa-users fa-1x"></i>Manage Users</a>
            </li>

              <?php }
            ?>

            

            <?php 
                if ($_SESSION['utype'] == "A") 
                { ?>
                    
              
            <li>
                <a href="add_category.php" class="
                <?php
                    if($_SERVER['PHP_SELF']=='/filesaver/pages/add_category.php')
                    { 
                        echo 'active-menu';
                    }
                 ?>
                "><i class="fa fa-plus fa-1x"></i>Add Category</a>
            </li>

              <?php }
            ?>





            <?php 
                if ($_SESSION['uadd'] == "Y") 
                { ?>
                    
              
            <li>
                <a href="upload_file.php" class="
                <?php
                    if($_SERVER['PHP_SELF']=='/filesaver/pages/upload_file.php')
                    { 
                        echo 'active-menu';
                    }
                 ?>
                "><i class="fa fa-upload fa-1x"></i>File Upload</a>
            </li>

              <?php }
            ?>


            
            <li>
                <a href="user_files.php" class="
                <?php
                    if($_SERVER['PHP_SELF']=='/filesaver/pages/user_files.php' || $_SERVER['PHP_SELF']=='/filesaver/pages/viewfile.php')
                    { 
                        echo 'active-menu';
                    } 
                 ?>
                "><i class="fa fa-files-o fa-1x"></i>All Files</a>
            </li> 


            <?php 
                if ($_SESSION['utype'] == "A") 
                { ?>
                    
              
            <li>
                <a href="log.php" class="
                <?php
                    if($_SERVER['PHP_SELF']=='/filesaver/pages/log.php')
                    { 
                        echo 'active-menu';
                    }
                 ?>
                "><i class="fa fa-file-text-o fa-1x"></i>Log Book</a>
            </li>

              <?php }
            ?>


            <li>
                <a href="profile.php" class="
                <?php
                    if($_SERVER['PHP_SELF']=='/filesaver/pages/profile.php')
                    { 
                        echo 'active-menu';
                    }
                    else if($_SERVER['PHP_SELF']=='/filesaver/pages/edit_profile.php')
                    { 
                        echo 'active-menu';
                    } 
                 ?>
                "><i class="fa fa-user fa-1x"></i>My Profile</a>
            </li> 

            
        </ul>
    </div>
</nav>  
</div>
<!-- /. NAV SIDE  -->