<?php
    session_start();
    if (!isset($_SESSION['name']))
    {
        header('Location: ./index.php');
    }
    require("../config.php");
?>
<?php
if (isset($_POST['category_name']) && isset($_POST['remark']) && $_POST['remark']!="" && $_POST['category_name']!="") 
{
  $category_name = $_POST['category_name'];
  $remark = $_POST['remark'];
  $sql = "INSERT INTO category (category_name, remark) VALUES ('$category_name', '$remark')";
  $result = mysql_query($sql,$conn);
  if ($result)
  {
      header('Location: add_category.php?cat=success');
  }
  else
  {
      header('Location: add_category.php?cat=fail');
  }
}

if (isset($_REQUEST['delete_cat']) && $_REQUEST['delete_cat']!="") 
{
  $id = $_REQUEST['delete_cat'];
  $sql = "DELETE FROM category WHERE id='$id' ";
  $result = mysql_query($sql,$conn);
  if ($result) 
  {
      header('Location: add_category.php?delete=success');
  }
  else
  {
      header('Location: add_category.php?delete=fail');

  }
}


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
      <link rel="icon" href="assets/images/icon.JPG">
      <?php include('includes/title.php'); ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <?php include('includes/menu.php'); ?>
        
        <div id="page-wrapper" >
            <div class="row">
                <div class="col-lg-offset-2 col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                              <form method="POST" enctype="multipart/form-data" action="add_category.php">
                                <div class="col-md-12">
                                  <?php
                                      if (isset($_GET['cat']) && $_GET['cat'] == "success")
                                          {
                                                  echo "
                                                  <div class='alert alert-success alert-dismissible fade in'>
                                                  <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                                  <strong>Category Added Successfully!</strong>.
                                                  </div>
                                                  ";
                                          }
                                          else if(isset($_GET['cat']) && $_GET['cat'] == "fail")
                                          {
                                              echo "
                                                  <div class='alert alert-danger alert-dismissible fade in'>
                                                  <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                                  <strong>Error!</strong> category not added.
                                                  </div>
                                                  ";
                                          }

                                    ?>
                                  <div class="col-md-12"><center><h3> Add New File Category </h3></center><hr></div>
                                   <div class="col-md-3"><label>Category Name <span style="color: red">*</span> </label></div>
                                      <div class="col-md-9">
                                        <div class="form-group">
                                          <input type="text" name="category_name" class="form-control text-left" placeholder="enter category name" required autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-lg-offset-0 col-md-3"><label>Remark <span style="color: red">*</span></label></div>
                                      <div class="col-md-9">
                                        <div class="form-group">
                                          <textarea class="form-control" name="remark" rows="3" placeholder="Enter category remark" required autocomplete="off"></textarea>
                                        </div>
                                    </div>
                                </div>
                                    
                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-2">
                                        <button type="submit" class="btn btn-primary" id="upload"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save Category</button>
                                    </div>
                                    <div class="col-lg-offset-1 col-lg-2">
                                        <a href="add_category.php" class="btn btn-warning"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a>
                                    </div>
                                    <div class="col-lg-offset-0 col-lg-2">
                                        <a href="index.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                                    </div>
                                </div>
                                
                              </form>
                            </div>
                        </div>
                    </div>

                     <!-- End Form Elements -->
                </div>
                <table class="table table-hover table-responsive table-bordered">
                  <tr>
                    <th>Category Id</th>
                    <th>Category Name</th>
                    <th>Category Remark</th>
                    <th>Action</th>
                  </tr>
                  <?php
                  $sql = "SELECT * FROM category";
                  $result = mysql_query($sql,$conn);
                  while($row = mysql_fetch_assoc($result))
                  { ?>
                    <tr>
                      <td><?=$row['id']?></td>
                      <td><?=$row['category_name']?></td>
                      <td><?=$row['remark']?></td>
                      <td><a onclick="return confirm('Are You Sure?')" href="add_category.php?delete_cat=<?=$row['id']?>">Delete</a></td>
                    </tr>
                  <?php }
                  ?>
                </table>
        </div><!-- /. PAGE INNER  -->   
    	</div><!-- /. page-wrapper  -->
    </div><!-- /. wrapper  -->


    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
         <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
   
</body>
</html>
