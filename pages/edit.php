<?php
    session_start();
    $id = $_GET['id'];
    if (!isset($_SESSION['name']))
    {
        header('Location: ./index.php');
    }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
      <link rel="icon" href="assets/images/icon.JPG">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
   
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!-- multiple dropdown data-->
  <link href="assets/css/multiple-select.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
 <?php include('includes/menu.php'); ?>
        
        <div id="page-wrapper" >
            <div id="page-inner">
                <div>
                    <h2 class="text-center">Update User Information</h2>
                    <hr>
                </div>
                <?php 
                    require("../config.php");
                    $sql = "SELECT * FROM user where userid = '$id' ";
                    $result = mysql_query($sql,$conn);
                    $row = mysql_fetch_assoc($result);
                    $reporting = $row['reporting'];
                ?>
                <form action="update_user_profile.php?id=<?php echo $id ?>" method="POST" name="RegForm" onsubmit="return validate()">
              
              <div class="col-md-3 text-left"><label>User Id</label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <input type="text" class="form-control disabled" readonly value="<?php echo $row['userid'];?>">
                </div>
              </div>

              <div class="col-md-3 text-left"><label>Contact <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <input type="text" class="form-control" name="ucontact" value="<?php echo $row['ucontact'];?>" required>
                </div>
              </div>

              <div class="col-md-3 text-left"><label>Full Name <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <input type="text" class="form-control" name="ufullname" value="<?php echo $row['ufullname'];?>" required>
                </div>
              </div>

               <div class="col-md-3 text-left"><label>Email <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <input type="text" class="form-control" name="uemail" value="<?php echo $row['uemail'];?>" required>
                </div>
              </div>

              <div class="col-md-3 text-left"><label>User name <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <input type="text" class="form-control" name="uname" value="<?php echo $row['uname'];?>" required>
                </div>
              </div>

             <div class="col-md-3 text-left"><label>User Status <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">

                  <?php 
                    if ($row['usts'] == "A") 
                    { ?>
                      <select name="usts" class="form-control" required>
                      <option value="">--select--</option>
                      <option value="A" selected="selected">Active</option>
                      <option value="D">Deactive</option>
                    </select>
                    <?php }
                    else
                    { ?>
                      <select name="usts" class="form-control">
                      <option value="">--select--</option>
                      <option value="A">Active</option>
                      <option value="D" selected="selected">Deactive</option>
                    </select>
                    <?php
                    }
                  ?>

                </div>
              </div>

              <div class="col-md-3 text-left"><label>Password <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <input type="password" class="form-control" name="upwd" value="<?php echo base64_decode($row['upwd']);?>" required>
                </div>
              </div>

              <div class="col-md-3 text-left"><label>User Type <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <!-- <input type="text" class="form-control" name="utype" value="<?php echo $row['utype'];?>"> -->
                  <?php 
                    if ($row['utype'] == "A") 
                    { ?>
                      <select name="utype" class="form-control" required>
                      <option value="">--select--</option>
                      <option value="A" selected="selected">Admin</option>
                      <option value="U">User</option>
                    </select>
                    <?php }
                    else
                    { ?>
                      <select name="utype" class="form-control" required>
                      <option value="">--select--</option>
                      <option value="A">Admin</option>
                      <option value="U" selected="selected">User</option>
                    </select>
                    <?php
                    }
                  ?>

                </div>
              </div>


              <div class="col-md-3 text-left"><label>Reporting<span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                    <select name="reporting" class="form-control" id="reporting">
                      <option value="">--select--</option>
                      <?php
                         require("../config.php");
                         $sql = "SELECT userid, ufullname FROM user where usts!='L' AND userid!='".$_SESSION['id']."' AND userid!='".$_REQUEST['id']."'  ";
                         $result = mysql_query($sql,$conn);
                         if (mysql_num_rows($result) > 0)
                         {
                            while ($row1 = mysql_fetch_assoc($result)) 
                            {
                                $id = $row1['userid'];
                                $name = $row1['ufullname']; 
                                
                                // echo '<option value="'.$id.'">'.$name.'</option>';
                                if (isset($_REQUEST['reporting']) && $_REQUEST['reporting']==$id) 
                                {
                                  echo '<option value="'.$id.'" selected>'.$name.'</option>';
                                }
                                else
                                {
                                  echo '<option value="'.$id.'">'.$name.'</option>';
                                }

                            }
                         }
                      ?>
                    </select>
                </div>
              </div>


              <div class="col-md-3 text-left"><a data-toggle="modal" data-target="#myModal">Category Permission</a> <span style="color: red">*</span></div>
              <div  class="col-md-6 text-left"><label style="color: white;"></label><div class="form-group"></div></div>


<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Category Permission</h4>
        </div>
        <div class="modal-body">
        	<div class="checkbox">
        	<input  type='checkbox' id='checkedAl' onclick="selectAlll()"/> <b style="font-size: 16px">Check all</b> <br>
        	</div>
        	<?php
	        	 $sql = "SELECT * FROM category";
                 $result = mysql_query($sql,$conn);
                 while ($row = mysql_fetch_assoc($result)) 
                 {
                 	$user = $_REQUEST['id'];
                 	$cid = $row['id'];
                 	 

                  ?>
                 	<div class="checkbox">
                 	<input id="<?php echo $row['id'] ; ?>" onchange="isChecked(this.value, <?php echo $_REQUEST['id'] ; ?>)" type='checkbox' name='checkedAll[]' class='checkSingle' value="<?php echo $row['id'] ; ?>"
                 	<?php
                 	$sql11 = "SELECT * FROM category_permission WHERE userid = $user AND category_id=$cid";
            	   $result11 = mysql_query($sql11,$conn);
            	   if (mysql_num_rows($result11) > 0)
            	   {
            	   	echo "checked";
            	   }
                 	?>


                 	 /><label style="font-size: 16px">&nbsp;<?=$row['category_name']?></label><br />
                 </div>
                <?php }
        	?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
 <!-- Modal -->

 <style type="text/css">
   	:checked + label {
  color: green;
  font-weight: bold;
}
   </style>
   <script type="text/javascript">
	function isChecked(checkbox, user) 
	{	
		if(document.getElementById(checkbox).checked==true){
			$.post("permission_category.php", {user:user, checkbox: checkbox, action:'cat_allow'}, function(data){
			});
		}else if(document.getElementById(checkbox).checked==false){
			$.post("permission_category.php", {user:user, checkbox: checkbox, action:'cat_disallow'}, function(data){
			});
		}
	}
   </script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
   <script type="text/javascript">
   	$(document).ready(function() {
  $("#checkedAl").change(function(){
    if(this.checked){
      $(".checkSingle").each(function(){
        this.checked=true;
      })              
    }else{
      $(".checkSingle").each(function(){
        this.checked=false;
      })              
    }
  });

  $(".checkSingle").click(function () {
    if ($(this).is(":checked")){
      var isAllChecked = 0;
      $(".checkSingle").each(function(){
        if(!this.checked)
           isAllChecked = 1;
      })              
      if(isAllChecked == 0){ $("#checkedAl").prop("checked", true); }     
    }else {
      $("#checkedAl").prop("checked", false);
    }
  });
});
   </script>

 <script type="text/javascript">
   	function selectAlll() 
	{
	    checkboxes = document.getElementsByName('checkedAll[]');
	    selallbx=document.getElementById('checkedAl');
	    if(selallbx.checked==true)
		{
	      for(var i in checkboxes)
		  {
	        if(checkboxes[i].checked == false)
			{
				checkboxes[i].click();
	        }
	      }
	    }
		else if(selallbx.checked==false)
		{
	      for(var i in checkboxes)
		  {
	        if(checkboxes[i].checked == true)
			{
				checkboxes[i].click();
	        }
	      }
	    }
	}
   </script>



              <div class="col-md-3"><label>Execute Permissions <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <?php 
                    if ($row['uexecute'] == "Y") 
                    { ?>
                      <select name="uexecute" class="form-control" onchange="if (this.value=='N'){permissions.style.visibility='visible'}else {permissions.style.visibility='hidden'};"  required>
                      <option value="">--select--</option>
                      <option value="Y" selected="selected">Yes</option>
                      <option value="N">No</option>
                    </select>
                    <?php }
                    else
                    { ?>
                      <select name="uexecute" class="form-control" onchange="if (this.value=='N'){permissions.style.visibility='visible'}else {permissions.style.visibility='hidden'};" required>
                      <option value="">--select--</option>
                      <option value="Y">Yes</option>
                      <option value="N" selected="selected">No</option>
                    </select>
                    
                    <?php
                    }
                  ?>
                </div>
              </div>

              <div id="sh" style="visibility:hidden;">
              <div class="col-md-3 text-left"><label>Add Permissions <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <!-- <input type="text" class="form-control" name="uadd" value="<?php echo $row['uadd'];?>"> -->
                  <?php 
                    if ($row['uadd'] == "Y") 
                    { ?>
                      <select name="uadd" class="form-control" required>
                      <option value="">--select--</option>
                      <option value="Y" selected="selected">Yes</option>
                      <option value="N">No</option>
                    </select>
                    <?php }
                    else
                    { ?>
                      <select name="uadd" class="form-control" required>
                      <option value="">--select--</option>
                      <option value="Y">Yes</option>
                      <option value="N" selected="selected">No</option>
                    </select>
                    <?php
                    }
                  ?>
                </div>
              </div>


              <div class="col-md-3 text-left"><label>View Permissions <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <!-- <input type="text" class="form-control" name="uview" value="<?php echo $row['uview'];?>"> -->
                  <?php 
                    if ($row['uview'] == "Y") 
                    { ?>
                      <select name="uview" class="form-control" required>
                      <option value="">--select--</option>
                      <option value="Y" selected="selected">Yes</option>
                      <option value="N">No</option>
                    </select>
                    <?php }
                    else
                    { ?>
                      <select name="uview" class="form-control" required>
                      <option value="">--select--</option>
                      <option value="Y">Yes</option>
                      <option value="N" selected="selected">No</option>
                    </select>
                    <?php
                    }
                  ?>
                </div>
              </div>

              <div class="col-md-3 text-left"><label>Delete Permissions <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <!-- <input type="text" class="form-control" name="udelete" value="<?php echo $row['udelete'];?>"> -->
                  <?php 
                    if ($row['udelete'] == "Y") 
                    { ?>
                      <select name="udelete" class="form-control" required>
                      <option value="">--select--</option>
                      <option value="Y" selected="selected">Yes</option>
                      <option value="N">No</option>
                    </select>
                    <?php }
                    else
                    { ?>
                      <select name="udelete" class="form-control" required>
                      <option value="">--select--</option>
                      <option value="Y">Yes</option>
                      <option value="N" selected="selected">No</option>
                    </select>
                    <?php
                    }
                  ?>

                </div>

              </div>
              </div>



              <div class="row">
              	<div class="col-lg-12"></div>
              	<div class="col-lg-12"></div>
              	<div class="col-lg-4"></div>
              <div class="col-md-3">
                <div class="form-group">
                  <button type="submit" name="update" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> update</button>
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <a href="manage_user.php" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
              </div>
              </div>



              </form>
          </div>    
      </div>
             <!-- /. PAGE INNER  -->
            </div>


    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>

         <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>

    <script>
    var permissions = document.getElementById('sh');
  </script>

   <script type="text/javascript">
      // Form validation code will come here.
      function validate()
      {
        var uname = document.forms["RegForm"]["uname"]; 
        var upwd = document.forms["RegForm"]["upwd"]; 
        var c_upwd = document.forms["RegForm"]["c_upwd"]; 
        var utype = document.forms["RegForm"]["utype"]; 
        var usts = document.forms["RegForm"]["usts"]; 
        var ucontact = document.forms["RegForm"]["ucontact"]; 
        var uemail = document.forms["RegForm"]["uemail"]; 
        var name = document.forms["RegForm"]["ufullname"];

        if (name.value == "" && uname.value == "" && c_upwd.value == "" && ucontact.value == "" && uemail.value == "" && upwd.value == "")                                 
        {
          window.alert("Please fill all required field.");
          name.focus();
          return false;
        }
        if (name.value == "")                                 
        {
          window.alert("Please enter full name.");
          name.focus();
          return false;
        }
        if (uname.value == "")                                 
        {
          window.alert("Please enter user name.");
          uname.focus();
          return false;
        }
        if (upwd.value == "")                                 
        {
          window.alert("Please enter password.");
          upwd.focus();
          return false;
        }
        if (upwd.value.length <= 4)                                 
        {
          window.alert("password should be greater than 4 digit.");
          upwd.focus();
          return false;
        }
        if (ucontact.value == "")                                 
        {
          window.alert("Please enter contact number.");
          ucontact.focus();
          return false;
        }

        if (ucontact.value.length != 10)                                 
        {
          window.alert("Invalid contact number.");
          ucontact.focus();
          return false;
        }
        if (uemail.value == "")                                 
        {
          window.alert("Please enter email id.");
          uemail.focus();
          return false;
        }

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(uemail.value) == false)                                 
        {
          window.alert("Invalid email id.");
          uemail.focus();
          return false;
        }
      }
</script>

 <?php 
if ($row['uexecute'] == "N") 
{ ?>
<script type="text/javascript">
  document.getElementById('sh').style.visibility='visible';
</script>
<?php } ?>   

               <!-- for multiple dropdown item select -->
  <script src="assets/js/multiple-select.js"></script>
  <script>
        $("#cat").multipleSelect({
            placeholder: "Select categories"
        });
    </script>
  <!-- for multiple dropdown item select -->
</body>
</html>
