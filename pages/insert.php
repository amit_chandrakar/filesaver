<?php
    session_start();
    if (!isset($_SESSION['name']))
    {
        header('Location: ./index.php');
    }
?>

<?php 
if (isset($_POST['update'])) 
{
  //echo "string";
    require("../config.php");
    $id = $_SESSION['id'];
    $uname = $_POST["uname"];
    $upwd = base64_encode($_POST["upwd"]);
    $utype = $_POST["utype"];
    $usts = $_POST["usts"];
    $ufullname = $_POST["ufullname"];
    $ucontact = $_POST["ucontact"];
    $uemail = $_POST["uemail"];
    $uexecute = $_POST["uexecute"];
    $reporting = $_POST["reporting"];
    $cat = $_POST["cat"];
    if ($uexecute == 'Y') 
    {
      $uadd = $_POST["uadd"];
      $udelete = $_POST["udelete"];
      $uview = $_POST["uview"];
      $uadd = "Y";
      $udelete = "Y";
      $uview = "Y";
    }
    else
    {
      $uadd = $_POST["uadd"];
      $udelete = $_POST["udelete"];
      $uview = $_POST["uview"];
    }
    
    $sql = "INSERT INTO user (uname, upwd, utype, usts, ufullname, ucontact, uemail, uexecute, uadd, udelete, uview, crby, reporting) VALUES ('$uname', '$upwd', '$utype', '$usts', '$ufullname', '$ucontact', '$uemail', '$uexecute' , '$uadd', '$udelete', '$uview', '$id', '$reporting')";
    $result = mysql_query($sql,$conn);
    $id=mysql_insert_id($conn);

    foreach ($_POST['cat'] as $clist) 
    {
        $r = mysql_query("INSERT INTO category_permission (userid, category_id) VALUES ('$id', '$clist')", $conn);
    }


    if ($result)
    {
      mkdir("upload/$id");
      header('Location: manage_user.php?insert=success');
      $log = "INSERT INTO `log` (`log_id`, `user_id`, `action`, `device_type`, `ip_address`, `date_time`) VALUES (NULL, '".$_SESSION['id']."', 'Add new user <b>$ufullname</b>', '".$_SERVER['HTTP_USER_AGENT']."', '".$_SERVER['REMOTE_ADDR']."', CURRENT_TIMESTAMP);";
      $log_result = mysql_query($log,$conn);
    }
    else
    {
      header('Location: manage_user.php?insert=fail');
      $log = "INSERT INTO `log` (`log_id`, `user_id`, `action`, `device_type`, `ip_address`, `date_time`) VALUES (NULL, '".$_SESSION['id']."', 'Failed Add new user <b>$ufullname</b>', '".$_SERVER['HTTP_USER_AGENT']."', '".$_SERVER['REMOTE_ADDR']."', CURRENT_TIMESTAMP);";
      $log_result = mysql_query($log,$conn);
    }
    
}


?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <link rel="icon" href="assets/images/icon.JPG">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin Panel</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

    <!-- multiple dropdown data-->
  <link href="assets/css/multiple-select.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <?php include('includes/menu.php'); ?>
        <div id="page-wrapper" >
            <div id="page-inner">
			
                <div class="row">
                    <h2 class="text-center">Add New User Information</h2><br>
                
                
                <form method="POST" name="RegForm" onsubmit="return validate()">
				
              <div class="col-md-3 text-left"><label>Full Name <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <input type="text" class="form-control" name="ufullname" id="ufullname" placeholder="Enter Full name" required autocomplete="off">
                </div>
              </div>
              

              <div class="col-md-3 text-left"><label>Contact <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <input type="text" class="form-control" name="ucontact" id="ucontact" placeholder="Enter Contact Number" required autocomplete="off">
                </div>
              </div>

              <div class="col-md-3 text-left"><label>User name <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <input type="text" class="form-control" name="uname" id="uname" placeholder="Enter User Name" required autocomplete="off">
                </div>
              </div>

              <div class="col-md-3 text-left"><label>Email <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <input type="text" class="form-control" name="uemail" id="uemail" placeholder="Enter Email Id" required autocomplete="off">
                </div>
              </div>

              <div class="col-md-3 text-left"><label>Password <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <input type="password" class="form-control" name="upwd" id="upwd" placeholder="Enter Password" required autocomplete="off">
                </div>
              </div>

              <div class="col-md-3 text-left"><label>User Status <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                    <select name="usts" class="form-control" id="u_status" required autocomplete="off">
                      <option value="">--select--</option>
                      <option value="A">Active</option>
                      <option value="D">Deactive</option>
                    </select>
                </div>
              </div>

              <div class="col-md-3 text-left"><label>Confirm Password <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                  <input type="password" class="form-control" name="c_upwd" id="c_upwd" placeholder="Enter Password" required autocomplete="off">
                </div>
              </div>


              <div class="col-md-3 text-left"><label>User Type <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                    <select name="utype" class="form-control" required id="u_type">
                      <option value="">--select--</option>
                      <option value="A">Admin</option>
                      <option value="U" selected>User</option>
                    </select>
                </div>
              </div>


              <div class="col-md-3 text-left"><label>Reporting<span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                    <select name="reporting" class="form-control" id="reporting">
                      <option value="">--select--</option>
                      <?php
                         require("../config.php");
                         $sql = "SELECT userid, ufullname FROM user where usts!='L'";
                         $result = mysql_query($sql,$conn);
                         if (mysql_num_rows($result) > 0)
                         {
                            while ($row = mysql_fetch_assoc($result)) 
                            {
                                $id = $row['userid'];
                                $name = $row['ufullname']; 
                                echo '<option value="'.$id.'">'.$name.'</option>';
                            }
                         }
                      ?>
                    </select>
                </div>
              </div>
              

              <div class="col-md-3 text-left"><label>Category Permission<span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                    <select name="cat[]" class="form-control" id="cat" multiple="multiple" >
                      <?php
                         require("../config.php");
                         $sql = "SELECT * FROM category";
                         $result = mysql_query($sql,$conn);
                         if (mysql_num_rows($result) > 0)
                         {
                            while ($row = mysql_fetch_assoc($result)) 
                            {
                                $category_name = $row['category_name'];
                                $id = $row['id']; 
                                echo '<option value="'.$id.'">'.'&nbsp;&nbsp;'.$category_name.'</option>';
                            }
                         }
                      ?>
                    </select>
                </div>
              </div>

 

              <div class="col-md-3 text-left"><label>Execute Permissions <span style="color: red">*</span></label></div>
              <div class="col-md-3 text-left">
                <div class="form-group">
                    <select id="e_per" required name="uexecute" class="form-control" onchange="if (this.value=='N'){permissions.style.visibility='visible'}else {permissions.style.visibility='hidden'};">
                      <option value="">--select--</option>
                      <option value="Y">Yes</option>
                      <option value="N">No</option>
                    </select>
                </div>
              </div>

				 <div id="sh" style="visibility:hidden;">
					  <div class="col-md-3 text-left"><label>Add Permissions <span style="color: red">*</span></label></div>
					  <div class="col-md-3 text-left">
						<div class="form-group">
							<select name="uadd" class="form-control">
							  <option value="">--select--</option>
							  <option value="Y" selected>Yes</option>
							  <option value="N">No</option>
							</select>
						</div>
					  </div>

					  <div class="col-md-3 text-left"><label>View Permissions <span style="color: red">*</span></label></div>
					  <div class="col-md-3 text-left">
						<div class="form-group">
							<select name="uview" class="form-control">
							  <option value="">--select--</option>
							  <option value="Y">Yes</option>
							  <option value="N" selected>No</option>
							</select>
						</div>
					  </div>

					  <div class="col-md-3 text-left"><label>Delete Permissions <span style="color: red">*</span></label></div>
					  <div class="col-md-3 text-left">
						<div class="form-group">
							<select name="udelete" class="form-control">
							  <option value="">--select--</option>
							  <option value="Y">Yes</option>
							  <option value="N" selected>No</option>
							</select>
						</div>
					  </div>
				  </div>
              

              <div class="col-lg-offset-3 col-md-4">
                <div class="form-group">
                <button type="submit" name="update" class="btn btn-success col-lg-6"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
              </div>
              </div>
			  
              <div class="col-md-4">
                <div class="form-group">
                  <a href="manage_user.php" class="btn btn-danger col-lg-6"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
              </div>
              

              </form>
			
			</div>
			</div><!-- /. PAGE INNER  -->    
		</div><!-- /. PAGE wrapper  -->
    </div><!-- /. wrapper  -->


    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    <script>
		var permissions = document.getElementById('sh');
	</script>
    <script type="text/javascript">
      // Form validation code will come here.
      function validate()
      {
        var uname = document.forms["RegForm"]["uname"]; 
        var upwd = document.forms["RegForm"]["upwd"]; 
        var c_upwd = document.forms["RegForm"]["c_upwd"]; 
        var utype = document.forms["RegForm"]["utype"]; 
        var usts = document.forms["RegForm"]["usts"]; 
        var ucontact = document.forms["RegForm"]["ucontact"]; 
        var uemail = document.forms["RegForm"]["uemail"]; 
        var name = document.forms["RegForm"]["ufullname"];

        if (name.value == "" && uname.value == "" && c_upwd.value == "" && ucontact.value == "" && uemail.value == "" && upwd.value == "")                                 
        {
          window.alert("Please fill all required field.");
          name.focus();
          return false;
        }
        if (name.value == "")                                 
        {
          window.alert("Please enter full name.");
          name.focus();
          return false;
        }
        if (uname.value == "")                                 
        {
          window.alert("Please enter user name.");
          uname.focus();
          return false;
        }
        if (upwd.value == "")                                 
        {
          window.alert("Please enter password.");
          upwd.focus();
          return false;
        }
        if (upwd.value.length <= 4)                                 
        {
          window.alert("password should be greater than 4 digit.");
          upwd.focus();
          return false;
        }
        if (c_upwd.value == "")                                 
        {
          window.alert("Confirm password.");
          c_upwd.focus();
          return false;
        }

        if (upwd.value != c_upwd.value)                                 
        {
          window.alert("Password not match.");
          c_upwd.focus();
          return false;
        }
        if (ucontact.value == "")                                 
        {
          window.alert("Please enter contact number.");
          ucontact.focus();
          return false;
        }

        if (ucontact.value.length != 10)                                 
        {
          window.alert("Invalid contact number.");
          ucontact.focus();
          return false;
        }

        if (uemail.value == "")                                 
        {
          window.alert("Please enter email id.");
          uemail.focus();
          return false;
        }

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(uemail.value) == false)                                 
        {
          window.alert("Invalid email id.");
          uemail.focus();
          return false;
        }
      }
</script>

               <!-- for multiple dropdown item select -->
  <script src="assets/js/multiple-select.js"></script>
  <script>
        $("#cat").multipleSelect({
            placeholder: "Select categories"
        });
    </script>
  <!-- for multiple dropdown item select -->


</body>
</html>





