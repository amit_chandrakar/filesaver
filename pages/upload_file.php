﻿<?php
    session_start();
    if (!isset($_SESSION['name'])){ header('Location: ./index.php'); }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
  <link rel="icon" href="assets/images/icon.JPG">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<!-- BOOTSTRAP STYLES-->
	<link href="assets/css/bootstrap.css" rel="stylesheet" />
	
	<!-- FONTAWESOME STYLES-->
	<link href="assets/css/font-awesome.css" rel="stylesheet" />
	
	<!-- CUSTOM STYLES-->
	<link href="assets/css/custom.css" rel="stylesheet" />
	
	<!-- GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
	
	<!-- multiple dropdown data-->
	<link href="assets/css/multiple-select.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <?php include('includes/menu.php'); ?>
        <div id="page-wrapper" >
            <div id="page-inner" style="margin-top: -10px;">
                <div class="row">
                    <div class="col-md-12">
                     <h3 class="f2">File Upload</h3> 
                    </div>
                    <div class="col-lg-12">
                      
                    <?php
                    if (isset($_GET['upload']) && $_GET['upload'] == "success")
                    {
                        echo "
                        <div class='alert alert-success alert-dismissible fade in'>
                        <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        <strong>Success!</strong> File Uploaded.
                        </div>
                        ";
                    }
                    else if(isset($_GET['upload']) && $_GET['upload'] == "fail")
                    {
                        echo "
                        <div class='alert alert-danger alert-dismissible fade in'>
                        <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        <strong>File not uploaded.!</strong>
                        </div>
                        ";
                    }
                    else if(isset($_GET['fupload']) && $_GET['fupload'] == "fail")
                    {
                        echo "
                        <div class='alert alert-danger alert-dismissible fade in'>
                        <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        <strong>Unsupported File Type.!</strong>
                        </div>
                        ";
                    }
                    else if (isset($_GET['file']) && $_GET['file'] == "success")
                    {
                        echo "
                        <div class='alert alert-success alert-dismissible fade in'>
                        <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        <strong>Files Deleted Successfully!</strong>
                        </div>
                        ";
                    }
                    else if(isset($_GET['file']) && $_GET['file'] == "fail")
                    {
                        echo "
                        <div class='alert alert-danger alert-dismissible fade in'>
                        <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        <strong>Files not Deleted.!</strong>
                        </div>
                        ";
                    }
                    ?>
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               <div class="row">
                <div class="col-lg-offset-2 col-md-8" style="margin-top: -40px">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">
                            Form Element Examples
                        </div> -->
                        <div class="panel-body">
                            <div class="row">
                              <form method="POST" enctype="multipart/form-data" action="upload.php" name="RegForm" onsubmit="return validate()">
                                <div class="col-md-12">

                                   <div class="col-md-3"><label>File Name <span style="color: red">*</span> </label></div>
                                      <div class="col-md-9">
                                        <div class="form-group">
                                          <input type="text" name="filename" class="form-control text-left" placeholder="enter file name" required autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-lg-offset-0 col-md-3"><label>Remark <span style="color: red">*</span></label></div>
                                      <div class="col-md-9">
                                        <div class="form-group">
                                          <textarea class="form-control" name="remark" rows="3" placeholder="Enter file upload remark" required autocomplete="off"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-3"><label>Upload File</label></div>
                                      <div class="col-md-9 text-left">
                                        <div class="form-group">
                                          <input type="file" name="upload[]" multiple onchange="ValidateSingleInput(this); preview_images();" onclick="clear();" id="upload" class="form-control" required ><span style="color: red;">only .jpg, .jpeg, .pdf, .xlsx, .xls, .csv, ".doc", .docx, .png, .cdr files are allow</span>
                                        </div>
                                    </div>
                                    <input type="hidden" name="i" value="" id="value">
                                    <div class="col-md-12" id="image_preview"></div>



                                    <div class="col-md-3"><label>Select Category</label></div>
                                      <div class="col-md-9" style="margin-bottom: 10px;">
                                        <div class="form-group">
                                          <select class="col-md-7 col-xs-12 form-control" name="cat_list">
                                              <?php
                                                require("../config.php");
                                                if ($_SESSION['utype']=='A') 
                                                {
                                                  $sql = "SELECT * FROM `category`";
                                                } 
                                                else
                                                {
                                                  $sql = "SELECT * FROM `category` c INNER JOIN category_permission cp ON c.id=cp.category_id WHERE cp.userid=".$_SESSION['id'];
                                                }
                                                
                                                $result = mysql_query($sql,$conn);
                                           if (mysql_num_rows($result) > 0)
                                           {
                                              // echo "<option>Select Category<option>";
                                              while ($row = mysql_fetch_assoc($result)) 
                                              {
                                                  $cid = $row['id'];
                                                  $category_name = $row['category_name']; 
                                                  echo '<option value="'.$cid.'">'.$category_name.'</option>';
                                              }
                                           }
                                              ?>
                                          </select>
                                        </div>
                                    </div>




                                    
                                    	<div class="col-md-3"><label>Select Users</label></div>
                                      <div class="col-md-8" style="margin-bottom: 10px;">
                                        <div class="form-group">
                                          <select multiple="multiple" class="col-md-5 col-xs-12 form-control user_name" name="up_list[]">
                                              <?php
                                              	require("../config.php");
                                              	$id=$_SESSION['id'];
                                              	$sql = "SELECT * FROM user where usts!='L' AND usts!='D' AND userid!=$id";
                                              	$result = mysql_query($sql,$conn);
		                                       if (mysql_num_rows($result) > 0)
		                                       {
		                                          while ($row = mysql_fetch_assoc($result)) 
		                                          {
		                                              $uid = $row['userid'];
		                                              $name = $row['ufullname']; 
		                                              echo '<option value="'.$uid.'">'."&nbsp;&nbsp;".$name.'</option>';
		                                          }
		                                       }
                                              ?>
                                          </select>
                                        </div>
                                    </div>

									</div>
                                    

                                    <div class="row">
                                        <div class="col-lg-offset-2 col-lg-2">
                                            <button type="submit" class="btn btn-primary"name="save" id="upload"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save File</button>
                                        </div>
                                        <div class="col-lg-offset-1 col-lg-2">
                                            <a href="upload_file.php" class="btn btn-warning"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a>
                                        </div>
                                        <div class="col-lg-offset-1 col-lg-2">
                                            <a href="index.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                                        </div>

                                    </div>
                                
                              </form>
                            </div>
                        </div>
                    </div>

                     <!-- End Form Elements -->
                </div>
            
			<div class="row">
				<div class="col-lg-12">
					<h3>Recent Files Uploads (Last 5)</h3>
					<div class="table-responsive">
						<table class="table table-bordered">
							<tr class="text-center">
							  <th>File Id</th>
							  <th>File Name</th>
							  <th>Remark</th>
							  <th>Uploaded date</th>
							  <th>Action</th>
							</tr>
									
							<?php 
							require("../config.php");
							$uid = $_SESSION['id'];
							$sql = "SELECT user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate, user_subfile.fup2name, user_subfile.f2name FROM user_file INNER JOIN user_subfile ON user_file.fileid = user_subfile.fileid where user_file.crby= '".$uid."' group by user_subfile.fileid order by user_file.fileid desc limit 0,5";
							$result = mysql_query($sql,$conn);
							if (mysql_num_rows($result) > 0)
							{
								while ($row = mysql_fetch_assoc($result)) 
								{ ?>
								<tr>
									<td><?php echo $row['fileid'];?></td>
									<td><?php echo $row['fname'];?></td>
									<td><?php echo $row['fremark'];?></td>
									<td><?php echo date('d-m-Y', strtotime($row['crdate'])); ?></td>
                  
									<td>
									<a href="viewfile.php?fileid=<?php echo $row['fileid']; ?>&date=<?php echo $row['crdate']; ?>&user=own">View</a>
									<?php 
                    
                    $date1=date_create(date('Y-m-d'));
                    $date2=date_create(date('d-m-Y', strtotime($row['crdate'])));
                    $diff=date_diff($date1,$date2);
                    $diff->format("%a");


									if( ($_SESSION['uexecute'] == 'Y' || $_SESSION['udelete'] == 'Y') && $diff->format("%a")==0)
									{ ?>
									| <a onclick="return confirm('Do you wany to delete this file..?')" href="delete_fileid_upload_file.php?fileid=<?=$row['fileid']?>"> Delete</a>
									<?php }
									?>
									</td>
								</tr>
								<?php }
							}
							?>
						</table>
					</div>
				</div>
			</div>
			
            
			
        </div><!-- /. PAGE INNER  -->
            </div><!-- /. PAGE WRAPPER  -->
        </div><!-- /. WRAPPER  -->


	<!-- JQUERY SCRIPTS -->
	<script src="assets/js/jquery-1.10.2.js"></script>
	
	<!-- BOOTSTRAP SCRIPTS -->
	<script src="assets/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="http://www.expertphp.in/js/jquery.form.js"></script>

	<!-- METISMENU SCRIPTS -->
	<script src="assets/js/jquery.metisMenu.js"></script>
	
	<!-- CUSTOM SCRIPTS -->
	<script src="assets/js/custom.js"></script>

    <script type="text/javascript">
      // Form validation code will come here. 
      function validate()
      {
        var filename = document.forms["RegForm"]["filename"]; 
        var remark = document.forms["RegForm"]["remark"];

        if (filename.value == "" && remark.value == "")                                 
        {
          window.alert("Please fill all required field.");
          filename.focus();
          return false;
        }
        if (filename.value == "")                                 
        {
          window.alert("Please enter file name.");
          filename.focus();
          return false;
        }
        if (remark.value == "")                                 
        {
          window.alert("Please enter remark.");
          remark.focus();
          return false;
        }
      }
	</script>
	
	<script type="text/javascript">
		var _validFileExtensions = [".jpg", ".jpeg", ".pdf", ".xlsx", ".xls", ".csv", ".doc", ".docx", ".png", ".cdr"];    
		function ValidateSingleInput(oInput) 
		{
			if (oInput.type == "file") 
			{
				var sFileName = oInput.value;
				 if (sFileName.length > 0) 
				 {
					var blnValid = false;
					for (var j = 0; j < _validFileExtensions.length; j++) 
					{
						var sCurExtension = _validFileExtensions[j];
						if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) 
						{
							blnValid = true;
							break;
						}
					}
					if (!blnValid) 
					{
						alert("File type is not allowed. Allow extensions are: " + _validFileExtensions.join(", "));
						oInput.value = "";
						return false;
					}
				}
			}
			return true;
		}
	</script>

	<!-- for multiple dropdown item select -->
	<script src="assets/js/multiple-select.js"></script>
	<script>
        $(".user_name").multipleSelect({
            placeholder: "Select Users"
        });
    </script>
	<!-- for multiple dropdown item select -->

	<!-- for multiple image selection -->
	<script>
	  function preview_images() 
	  {
      document.getElementById("image_preview").innerHTML = "";
		var total_file=document.getElementById("upload").files.length;
		document.getElementById("value").value=total_file;
		for(var i=0;i<total_file;i++)
		{
		  $('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'><input type='text' name='test"+i+"' class='form-control' style='margin-top:5px; margin-bottom:5px' placeholder='sub-file here'> </div>");
		}
	  }
	  jQuery(document).ready(function()
	  {
		jQuery('#image_preview').live('click', function(event) 
		{        
		  jQuery('#upload').hide()
		});
	  });
	</script>
	<!-- for multiple image selection -->

</body>
</html>
