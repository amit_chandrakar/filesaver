﻿<?php
   session_start();
   if (!isset($_SESSION['name']))
   {
       header('Location: ./index.php');
   }
	$id = $_SESSION['id'];
?>
<!DOCTYPE html>
<head>
   <meta charset="utf-8" />
   <link rel="icon" href="assets/images/icon.JPG">
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <!-- BOOTSTRAP STYLES-->
   <link href="assets/css/bootstrap.css" rel="stylesheet" />
   <!-- FONTAWESOME STYLES-->
   <link href="assets/css/font-awesome.css" rel="stylesheet" />
   <!-- CUSTOM STYLES-->
   <link href="assets/css/custom.css" rel="stylesheet" />
   <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   <!-- chosen dropdown -->
   <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css">
   <!-- <script src="http://code.jquery.com/jquery-1.8.3.js"></script> -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

</head>
<body>
   <div id="wrapper">
      <?php include('includes/menu.php'); ?>
      <div id="page-wrapper" >
         <div id="page-inner">
            <div class="row">
               <?php 
                  $fileid = $_REQUEST['fileid'];
                ?>
                <div class="col-lg-12">
                  <?php
                     require("../config.php");
                     $sql = "SELECT * FROM user_file where fileid = $fileid";
                     $result = mysql_query($sql,$conn);
                     $row = mysql_fetch_assoc($result);
                      $fname = $row['fname'];
                      $fremark = $row['fremark'];
                  ?>
                  <div class="col-lg-3"><b>File Id:</b> <?php echo $fileid; ?></div>
                  <div class="col-lg-3"><b>File Name:</b> <?php echo $fname; ?></div>
                  <div class="col-lg-4"><b>File Remark:</b> <?php echo $fremark; ?></div>

                </div>
            </div><!-- /. ROW  -->
            <hr />

            
            <form name="access_form" method="post" action="permission.php">
            
            <?php
            require("../config.php");
            $sql = "SELECT * FROM user WHERE userid!=$id";
            $result = mysql_query($sql,$conn);
            if (mysql_num_rows($result) > 0)
            { ?>
            	<div class="checkbox">
            	<input  type='checkbox' id='checkedAl' onclick="selectAlll()"/> <b style="font-size: 16px">Check all</b> <br>
            	</div>

               <?php

               
               while ($row = mysql_fetch_assoc($result)) 
               {
                   $uid = $row['userid'];
                   $name = $row['ufullname'];
                   $sql11 = "SELECT * FROM admin_file WHERE fileid = $fileid AND userid=$uid";
            	   $result11 = mysql_query($sql11,$conn);
            	   if (mysql_num_rows($result11) > 0) 
            	   { ?>
            	   		<div class="checkbox">
                   		<input id="chk<?php echo $uid ; ?>" checked="checked" type='checkbox' name='checkedAll[]' class='checkSingle' value="<?php echo $uid ; ?>" onchange="isChecked(this.value, <?php echo $fileid ; ?>)" /><label style="font-size: 16px"> <?php echo $name ;?></label><br />
                   		</div>
            	   <?php }
            	   else
            	   {
                   ?>
                   <div class="checkbox">
                   		<input id="chk<?php echo $uid ; ?>" type='checkbox' name='checkedAll[]' class='checkSingle' value="<?php echo $uid ; ?>" onchange="isChecked(this.value, <?php echo $fileid ; ?>)" /><label style="font-size: 16px"> <?php echo $name ;?></label><br />
                   		</div>
                   <?php
                   }
               }
            }
          ?>
          <input type="hidden" name="fileid" value="<?php echo $fileid; ?>">
          <a href="javascript:history.go(-1)" class="btn btn-warning">Back</a>
          <a href="file_permission.php?fileid=<?php echo $fileid; ?>" class="btn btn-danger">Refresh</a>

          </form>
         </div><!-- /. PAGE INNER  -->
      </div><!-- /. PAGE WRAPPER  -->
   </div><!-- /. WRAPPER  -->
   
   
   
   <!-- BOOTSTRAP SCRIPTS -->
   <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
   <script type="text/javascript">
   	$(document).ready(function() {
  $("#checkedAl").change(function(){
    if(this.checked){
      $(".checkSingle").each(function(){
        this.checked=true;
      })              
    }else{
      $(".checkSingle").each(function(){
        this.checked=false;
      })              
    }
  });

  $(".checkSingle").click(function () {
    if ($(this).is(":checked")){
      var isAllChecked = 0;
      $(".checkSingle").each(function(){
        if(!this.checked)
           isAllChecked = 1;
      })              
      if(isAllChecked == 0){ $("#checkedAl").prop("checked", true); }     
    }else {
      $("#checkedAl").prop("checked", false);
    }
  });
});
   </script>

   <style type="text/css">
   	:checked + label {
  color: green;
  font-weight: bold;
}
   </style>
   <script type="text/javascript">
	function isChecked(checkbox, file) 
	{	
		if(document.getElementById('chk'+checkbox).checked==true){
			$.post("permission.php", {user:checkbox, fileid: file, action:'allow'}, function(data){
			});
		}else if(document.getElementById('chk'+checkbox).checked==false){
			$.post("permission.php", {user:checkbox, fileid: file, action:'disallow'}, function(data){
			});
		}
	}
   </script>

   <script type="text/javascript">
   	function selectAlll() 
	{
	    checkboxes = document.getElementsByName('checkedAll[]');
	    selallbx=document.getElementById('checkedAl');
	    if(selallbx.checked==true)
		{
	      for(var i in checkboxes)
		  {
	        if(checkboxes[i].checked == false)
			{
				checkboxes[i].click();
	        }
	      }
	    }
		else if(selallbx.checked==false)
		{
	      for(var i in checkboxes)
		  {
	        if(checkboxes[i].checked == true)
			{
				checkboxes[i].click();
	        }
	      }
	    }
	}
   </script>

</body>
</html>