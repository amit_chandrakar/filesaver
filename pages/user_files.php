﻿<?php
   session_start();
   if (!isset($_SESSION['name']))
   {
       header('Location: ./index.php');
   }
$id = $_SESSION['id'];
?>
<!DOCTYPE html>
<head>
   <meta charset="utf-8" />
   <link rel="icon" href="assets/images/icon.JPG">
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <!-- BOOTSTRAP STYLES-->
   <link href="assets/css/bootstrap.css" rel="stylesheet" />
   <!-- FONTAWESOME STYLES-->
   <link href="assets/css/font-awesome.css" rel="stylesheet" />
   <!-- CUSTOM STYLES-->
   <link href="assets/css/custom.css" rel="stylesheet" />
   <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   <!-- chosen dropdown -->
   <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css">
   <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
   <script type="text/javascript">
      $(function() 
      {
        $(".chzn-select").chosen();
      });
   </script>
   <!-- chosen dropdown -->
   <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>
   <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
   <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
   <style type="text/css">
      .chosen-container-single .chosen-single 
      {
        position: relative;
        display: block;
        overflow: hidden;
        padding: 0 0 0 8px;
        height: 33px;
        border: 1px solid #aaa;
        border-radius: 0px;
        background-color: #fff;
        background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(20%, #ffffff), color-stop(50%, #f6f6f6), color-stop(52%, #eeeeee), color-stop(100%, #f4f4f4));
        background: -webkit-linear-gradient(top, #ffffff 20%, #f6f6f6 50%, #eeeeee 52%, #f4f4f4 100%);
        background: -moz-linear-gradient(top, #ffffff 20%, #f6f6f6 50%, #eeeeee 52%, #f4f4f4 100%);
        background: -o-linear-gradient(top, #ffffff 20%, #f6f6f6 50%, #eeeeee 52%, #f4f4f4 100%);
        background: linear-gradient(top, #ffffff 20%, #f6f6f6 50%, #eeeeee 52%, #f4f4f4 100%);
        background-clip: padding-box;
        box-shadow: 0 0 3px white inset, 0 1px 1px rgba(0, 0, 0, 0.1);
        color: #444;
        text-decoration: none;
        white-space: nowrap;
        line-height: 30px;
      }
   </style>
</head>
<body>
   <div id="wrapper">
      <?php include('includes/menu.php'); ?>
      <div id="page-wrapper" >
         <div id="page-inner">
            <div class="row">
               
               <!-- Advanced Tables -->
               <div class="panel panel-default" style="margin-top: -20px">
                  <div class="panel-heading">
                     <!-- <center> <span class="h3" style="color: #428bca">My Files</span></center> -->
                     <div class="row">
                     	<div class="col-lg-12">
                     		<?php
				                  if (isset($_GET['file']) && $_GET['file'] == "success")
				                  {
				                    echo "
				                    <div class='alert alert-success alert-dismissible fade in'>
				                    <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
				                    <strong>Success!</strong> File Deleted.
				                    </div>
				                    ";
				                  }
				                  else if(isset($_GET['file']) && $_GET['file'] == "fail")
				                  {
				                    echo "
				                    <div class='alert alert-danger alert-dismissible fade in'>
				                    <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
				                    <strong>File Not Deleted.!</strong>
				                    </div>
				                    ";
				                  }
			                ?>
                     	</div>
                     </div>
                     <div class="row">
                           <div class="col-lg-3">
                              <div class="col-lg-11">
                                 <p><label>User Type</label></p>
                                 <select class="chzn-select form-control" name="ftype" id="ftype">
                                    <option value="">--select--</option>
                                    
                                    <option value="own" <?php if (isset($_REQUEST['ftype']) && $_REQUEST['ftype']=='own'){ echo "selected"; } ?> >Own Data</option>
                                    <?php
                                       require("../config.php");
                                       if ($_SESSION['utype'] == 'A') 
                                       {
                                          $sql = "SELECT userid, ufullname, usts FROM user where userid!=".$id;
                                       }
                                       else
                                       {
                                          $sql = "SELECT userid, ufullname, usts FROM user where reporting=$id AND userid!=".$id;
                                       }
                                       $result = mysql_query($sql,$conn);
                                       if (mysql_num_rows($result) > 0)
                                       {
                                          while ($row = mysql_fetch_assoc($result)) 
                                          {
                                              $uid = $row['userid'];
                                              $name = $row['ufullname']; 

                                              if (isset($_REQUEST['ftype']) && $_REQUEST['ftype']==$uid) 
                                              {
                                                echo '<option value="'.$uid.'" selected>'.$name.'</option>';
                                              }
                                              else
                                              {
                                                if ($row['usts']=='L') 
                                                { ?>
                                                  <option style='color:red' value="<?=$uid?>"><?= $name?> <?php echo "(Deleted)"; ?></option>
                                                <?php }
                                                else
                                                {
                                                  echo '<option value="'.$uid.'">'.$name.'</option>';
                                                }
                                              }


                                              
                                          }
                                          
                                          
                                       }


                                          if ($_SESSION['utype']=="U") 
                                          { 
                                            $check_userfile = "SELECT * FROM admin_file WHERE userid=".$id;
                                            $res = mysql_query($check_userfile, $conn);
                                            if (mysql_num_rows($res)>0) 
                                            {
                                            
                                            ?>

                                            
                                            <option value="A" <?php if (isset($_REQUEST['ftype']) && $_REQUEST['ftype']=='A'){ echo "selected"; } ?> >Admin</option>

                                          <?php } }



                                          if ($_SESSION['utype']=="U") 
                                          { ?>
                                            <option value="alll" <?php if (isset($_REQUEST['ftype']) && $_REQUEST['ftype']=='alll'){ echo "selected"; } ?> >All</option>
                                          <?php }



                                    
                                      if ($_SESSION['utype'] == 'A') 
                                       {
                                        if (isset($_REQUEST['ftype']) && $_REQUEST['ftype']=='all') 
                                        {
                                          echo "<option value='all' selected>All</option>";
                                        }
                                        else{
                                          echo "<option value='all'>All</option>";
                                        }
                                          
                                       }


                                       ?>
                                        }
                                    
                                 </select>
                              </div>
                           </div>
                           

                           <div class="col-lg-3">
                              <div class="col-lg-11">
                                 <p><label>Category</label></p>
                                 <select class="chzn-select form-control" name="category_name" id="category_name">
                                    <option value="">--select--</option>
                                    <?php
                                    if ($_SESSION['utype']=="U") 
                                    {
                                      $cat = "SELECT * FROM category c INNER JOIN category_permission cp ON c.id=cp.category_id WHERE cp.userid=".$_SESSION['id'];
                                    }
                                    else
                                    {
                                      $cat = "SELECT * FROM category";
                                    }
                                      $result = mysql_query($cat, $conn);
                                      while ($rr=mysql_fetch_assoc($result)) 
                                      {
                                        $id = $rr['id'];
                                        $category_name = $rr['category_name'];
                                        if (isset($_REQUEST['category']) && $_REQUEST['category']==$id) 
                                        {
                                          echo '<option value="'.$id.'" selected>'.$category_name.'</option>';
                                        }
                                        else
                                        {
                                          echo '<option value="'.$id.'" >'.$category_name.'</option>';
                                        }
                                      }
                                    ?>
                                 </select>
                              </div>
                           </div>


                           <div class="col-lg-3">
                              <p><label>From Date</label></p>
                              <div class="input-group">
                                 <input autocomplete="off" class="form-control" id="from_date" name="from_date" placeholder="MM/DD/YYYY" type="text" value="<?php if(isset($_REQUEST['from_date']) && $_REQUEST['from_date'] !=''){echo date("d-m-Y",strtotime($_REQUEST['from_date']));} ?>" />
                              </div>
                           </div>

                           <div class="col-lg-3">
                              <p><label>To Date</label></p>
                              <div class="input-group">
                                 <input autocomplete="off" class="form-control col-lg-6" id="to_date" name="to_date" placeholder="MM/DD/YYYY" type="text" value="<?php if(isset($_REQUEST['to_date']) && $_REQUEST['to_date']!=''){echo date("d-m-Y",strtotime($_REQUEST['to_date']));}?>" />
                              </div>
                           </div>

                           <div class="col-lg-3">
                              <div class="col-lg-11">
                                 <p><label>Shared Files</label></p>
                                 <select class="chzn-select form-control" name="shared_file" id="shared_file">
                                    <option value="">--select--</option>
                                    <?php
                                       require("../config.php");
                                       $sql = "SELECT * FROM user u INNER JOIN admin_file af ON af.created_by=u.userid WHERE u.userid!=1 AND af.userid='".$_SESSION['id']."' GROUP BY u.userid ";
                                       
                                       $result = mysql_query($sql,$conn);
                                       if (mysql_num_rows($result) > 0)
                                       {
                                          while ($row = mysql_fetch_assoc($result)) 
                                          {
                                              $uid = $row['created_by'];
                                              $name = $row['ufullname']; 
                                              echo '<option value="'.$uid.'">'.$name.'</option>'; 
                                          }
                                       }
                                       ?>
                                        }
                                 </select>
                              </div>
                           </div>

                           <script type="text/javascript">
                             function search()
                             { 
                              var x="user_files.php?shared_file="+document.getElementById("shared_file").value+"&ftype="+document.getElementById("ftype").value+"&category="+document.getElementById("category_name").value+"&from_date="+document.getElementById("from_date").value+"&to_date="+document.getElementById("to_date").value;
                              window.location=x;
                             }
                           </script>

                           <div class="col-lg-offset-8 col-lg-4">
                              <br>
                              <!-- <input type="submit" name="search" class="btn btn-primary" value="Search" onclick="search();"> -->
                              <button type="submit" name="search" class="btn btn-primary" value="Search" onclick="search();"><i class="fa fa-search"></i> Search</button>
                              <a class="btn btn-danger" href="user_files.php"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a>
                              <a class="btn btn-warning" href="index.php"><i class="fa fa-arrow-left"></i> Back</a>
                           </div>
                     </div>
                  </div>
                  <div class="panel-body">
                     <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered table-condensed">
                           <thead>
                              <tr>
                                 <?php
                                 if(isset($_REQUEST['ftype'])!='')
                                 {
                                  $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                  $to=date("Y-m-d", strtotime($_REQUEST['to_date']));
                                }
                                   if(isset($_REQUEST['ftype'])!='' && ($_REQUEST['ftype']=='all' || $_REQUEST['ftype']>0) && $from!='' && $to!='')
                                   {
                                      echo "<th>"."User Name"."</th>";
                                   }
                                 ?>
                                 <th>File Id</th>
                                 <th>File Name</th>
                                 <th>Remark</th>
                                 <th>Uploaded date</th>
                                 <th>Action / Permission</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php 
                                 require("../config.php");
                                 $id = $_SESSION['id'];
                                 if(isset($_REQUEST['ftype'])!='')
                                 {
                                
                                $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                $to=date("Y-m-d", strtotime($_REQUEST['to_date']));
                                
                                 if($_REQUEST['ftype']=='all' && $_REQUEST['from_date']!='' && $_REQUEST['to_date']!='')
                                 {
                                    $id = $_SESSION['id'];
                                    $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby where user.userid!= '".$id."' AND  user_file.crdate between '".$from."' AND '".$to."' group by userid desc",$conn);

                                 }

                                 elseif($_REQUEST['ftype']=='alll')
                                 {
                                    $id = $_SESSION['id'];
                                    $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file, user, admin_file af where user_file.crby='$id' or (user_file.crby=af.fileid and af.userid='$id') or (user_file.crby=user.userid and user.reporting='$id') group by fileid desc",$conn);

                                 }

                                 else if(isset($_REQUEST['category']) && $_REQUEST['category']!="" && $_REQUEST['ftype']=='all' && $_REQUEST['from_date']!='' && $_REQUEST['to_date']!='')
                                 {
                                    $id = $_SESSION['id'];
                                    
                                    $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby where user_file.category='".$_REQUEST['category']."' AND user.userid!= '".$id."' AND  user_file.crdate between '".$from."' AND '".$to."' group by userid desc",$conn);

                                 }

                                 else if(isset($_REQUEST['category']) && $_REQUEST['category']!="" && $_REQUEST['ftype']=='' && $_REQUEST['from_date']=='' && $_REQUEST['to_date']=='')
                                 {
                                    $id = $_SESSION['id'];
                                    
                                    $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby where user_file.category='".$_REQUEST['category']."' AND user.userid!= '".$id."' group by userid desc",$conn);

                                 }


                                 else if(isset($_REQUEST['category']) && $_REQUEST['category']!="" && $_REQUEST['ftype']=='' && $_REQUEST['from_date']!='' && $_REQUEST['to_date']!='')
                                 {
                                    $id = $_SESSION['id'];
                                    
                                    $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby where user_file.category='".$_REQUEST['category']."' AND user.userid!= '".$id."' AND user_file.crdate between '".$from."' AND '".$to."' group by userid desc",$conn);

                                 }


                                 elseif($_REQUEST['ftype']>0 && $_REQUEST['from_date']!='' && $_REQUEST['to_date']!='')
                                 {
                                    $id = $_SESSION['id'];

                                   $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                   $to=date("Y-m-d", strtotime($_REQUEST['to_date']));

                                   $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby where user_file.crdate between '".$from."' AND '".$to."' AND user_file.crby=".$_REQUEST['ftype']." ",$conn);
                                 }

                                  elseif($_REQUEST['shared_file']>0 && $_REQUEST['from_date']=='' && $_REQUEST['to_date']=='')
                                 {
                                    $id = $_SESSION['id'];

                                   $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                   $to=date("Y-m-d", strtotime($_REQUEST['to_date']));

                                   $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby INNER JOIN admin_file ON admin_file.fileid=user_file.fileid where user_file.crby=".$_REQUEST['shared_file']." ",$conn);
                                 }

                                 elseif($_REQUEST['shared_file']>0 && $_REQUEST['from_date']!='' && $_REQUEST['to_date']!='')
                                 {
                                    $id = $_SESSION['id'];

                                   $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                   $to=date("Y-m-d", strtotime($_REQUEST['to_date']));

                                   $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby INNER JOIN admin_file ON admin_file.fileid=user_file.fileid where user_file.crdate between '".$from."' AND '".$to."' AND user_file.crby=".$_REQUEST['shared_file']." ",$conn);
                                 }

                                 elseif($_REQUEST['ftype']>0 && $_REQUEST['from_date']!='' && $_REQUEST['to_date']!='' && $_REQUEST['category']!="")
                                 {
                                    $id = $_SESSION['id'];

                                   $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                   $to=date("Y-m-d", strtotime($_REQUEST['to_date']));

                                   $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby where user_file.category='".$_REQUEST['category']."' AND user_file.crdate between '".$from."' AND '".$to."' AND user_file.crby=".$_REQUEST['ftype']." ",$conn);
                                 }

                                 elseif($_REQUEST['category']!='' && $_REQUEST['ftype']=='own' && $_REQUEST['from_date']!='' && $_REQUEST['to_date']!='')
                                 {
                                    $id = $_SESSION['id'];

                                   $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                   $to=date("Y-m-d", strtotime($_REQUEST['to_date']));
                                   $category = $_REQUEST['category'];
                                   // echo "dsavdsv"; 
                                   $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby where user_file.category='$category' AND user_file.crdate between '$from' AND '$to' AND user_file.crby='$id' order by crdate desc",$conn);
                                 }

                                 elseif($_REQUEST['category']!='' && $_REQUEST['ftype']=='own' && $_REQUEST['from_date']=='' && $_REQUEST['to_date']=='' && $_REQUEST['shared_file']=='')
                                 {
                                    $id = $_SESSION['id'];
                                    // echo "dsavdsv"; 
                                   $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                   $to=date("Y-m-d", strtotime($_REQUEST['to_date']));
                                   $category = $_REQUEST['category'];
                                   $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby where user_file.category='$category' AND user_file.crby='$id' order by crdate desc",$conn);
                                 }

                                 elseif($_REQUEST['ftype']=='own' && $_REQUEST['from_date']!='' && $_REQUEST['shared_file']=='' && $_REQUEST['category']=='' && $_REQUEST['to_date']!='')
                                 {
                                    $id = $_SESSION['id'];

                                   $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                   $to=date("Y-m-d", strtotime($_REQUEST['to_date']));
                                   $category = $_REQUEST['category'];
                                  
                                   $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby where  user_file.crdate between '$from' AND '$to' AND user_file.crby='$id' order by crdate desc",$conn);
                                 }

                                 elseif($_REQUEST['ftype']=='own' && $_REQUEST['from_date']=='' && $_REQUEST['to_date']=='' && $_REQUEST['category']=='' && $_REQUEST['shared_file']=='' )
                                 {
                                    $id = $_SESSION['id'];

                                   $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                   $to=date("Y-m-d", strtotime($_REQUEST['to_date']));
                                   $category = $_REQUEST['category'];
                                  
                                   $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby where user_file.crby='$id' order by crdate desc",$conn);

                                 }


                                 

                                 elseif($_REQUEST['category'] && $_REQUEST['from_date']!='' && $_REQUEST['to_date']!='')
                                 {
                                    $id = $_SESSION['id'];

                                   $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                   $to=date("Y-m-d", strtotime($_REQUEST['to_date']));

                                   $result = mysql_query("SELECT user_file.crby, user.ufullname, user_file.fileid, user_file.fname, user_file.fremark, user_file.crdate FROM user_file INNER JOIN user ON user.userid = user_file.crby where user_file.crdate between '".$from."' AND '".$to."' AND user_file.category=".$_REQUEST['category']." ",$conn);
                                 }

                                 

                                 elseif($_REQUEST['ftype']=='A' && $_REQUEST['from_date']!='' && $_REQUEST['to_date']!='')
                                 {

                                    $result = mysql_query("SELECT `admin_file`.`fileid`, `user_file`.`fname`, `user_file`.`fremark`, `user_file`.`crdate` FROM `user_file` INNER JOIN `admin_file` ON `admin_file`.`userid` = '$id' INNER Join `user_subfile` ON `admin_file`.`fileid` = `user_subfile`.`fileid` where  `admin_file`.`fileid` = `user_file`.`fileid` AND `user_file`.`crdate` between '".$from."' AND '".$to."' GROUP BY  `admin_file`.`fileid` DESC",$conn);
                                     
                                 }

                                 if(mysql_num_rows($result) > 0)
                                 {
                                     while ($row = mysql_fetch_assoc($result)) 
                                     {
                                        ?>
                                  <tr>
                                 <?php

                                 if(($_REQUEST['ftype']=='all' || $_REQUEST['ftype']>0) && $from!='' && $to!='')
                                 {
                                  $crby = $row['crby'];
                                  $check = mysql_query("SELECT * from user where reporting = $crby",$conn);
                                  $row1 = mysql_fetch_assoc($check);
                                  if ($row1['reporting']>0) 
                                  {
                                    $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                   $to=date("Y-m-d", strtotime($_REQUEST['to_date']));
                                   ?>
                                   <td><a  href="" onclick='window.open("allusers.php?userid=<?php echo $crby ; ?>&from_date=<?php echo $from; ?>&to_date=<?php echo $to; ?>", "_blank", "toolbar=yes,top=500,left=500,width=900,height=400"); return false;' data-toggle='tooltip' title='See Reportings'><?php echo $row['ufullname']; ?></a></td>

                                <?php  }
                                  else
                                  {
                                    echo "<td>".$row['ufullname']."</td>";
                                  }
                                 }
                                 
                                 ?>
                                 <td><?php echo $row['fileid'];?></td>
                                 <td><?php echo $row['fname'];?></td>
                                 <td><?php echo $row['fremark'];?></td>
                                 <td><?php echo date('d-m-Y', strtotime($row['crdate']));?></td>
                                 <td>
                                    <a data-toggle='tooltip' title='View Files' href="viewfile.php?fileid=<?php echo $row['fileid']; ?>&date=<?php echo $row['crdate']; ?>&filename=<?php echo $row['fname']; ?>&user=<?=$_REQUEST['ftype']?>&shared_file=<?=$_REQUEST['shared_file']?>">View</a>
                                    
                                    <?php 
                                      $date1=date_create(date('Y-m-d'));
                                      $date2=date_create(date('d-m-Y', strtotime($row['crdate'])));
                                      $diff=date_diff($date1,$date2);
                                      $diff->format("%a");

                                    if ($_REQUEST['ftype']=="own" && $diff->format("%a")==0 && ($_SESSION['udelete']=='Y' || $_SESSION['uexecute']=='Y')) 
                                       { ?>
                                    | <a href="delete_fileid.php?fileid=<?php echo $row['fileid'];?>&from_date=<?php echo $_REQUEST['from_date'];?>&to_date=<?php echo $_REQUEST['to_date'];?>&fupdate=<?php echo $row['crdate'];?>" data-toggle='tooltip' title='Delete Files'>Delete </a>
                                    <?php }
                                       ?>

                                      <?php if ($_REQUEST['ftype']=="own") 
                                      { ?> |
                                        <a href="file_permission.php?fileid=<?php echo $row['fileid'];?>" data-toggle='tooltip' title='Give file permission'>Permission</a>
                                      <?php }
                                      ?>

                                    
                                 </td>
                              </tr>
                              <?php 
                                 }
                                 }

                                 //-------------------------------------------------------------------
                                 	else
                                 	{
                                 		if($_REQUEST['ftype']>0 && $from!='' && $to!='')
                                 		{
                                 			echo "<tr>";
                                 			$ftype = $_REQUEST['ftype'];
                                 			$check = mysql_query("SELECT * from user where userid = $ftype",$conn);
                                  			$row = mysql_fetch_assoc($check);

                                  			$sql1 = "SELECT uf.fileid, u.userid FROM user_file uf, user u where u.reporting=".$_REQUEST['ftype']." and u.userid=uf.crby";
										   $result1 = mysql_query($sql1,$conn);
										   $row1 = mysql_fetch_assoc($result1);
										   if (mysql_num_rows($result1)>0) 
										   { ?>
											<td>
											<a  href="" onclick='window.open("allusers.php?userid=<?php echo $row['userid'] ; ?>&from_date=<?php echo $from; ?>&to_date=<?php echo $to; ?>", "_blank", "toolbar=yes,top=500,left=500,width=900,height=400"); return false;' data-toggle='tooltip' title='See Reportings'><?php echo $row['ufullname']; ?></a>
											</td>

										   <?php }
										   	else
										   	{
										   		echo "<td>".$row['ufullname']."</td>";
										   	}
                                  			?>
                                  					
                                  					<td>NONE</td>
                                  					<td>NONE</td>
                                  					<td>NONE</td>
                                  					<td>NONE</td>
                                  					<td>NONE</td>
                                  				</tr>

                                  			<?php

                                 		}
                                 	}
                               

                               //-------------------------------------------------------------------
                                } ?>
                           </tbody>
                           <tfoot>
                              <tr>
                                 <?php
                                 if(isset($_REQUEST['ftype'])!='')
                                 {
                                  $from=date("Y-m-d", strtotime($_REQUEST['from_date']));
                                  $to=date("Y-m-d", strtotime($_REQUEST['to_date']));
                                }
                                   if(isset($_REQUEST['ftype'])!='' && ($_REQUEST['ftype']=='all' || $_REQUEST['ftype']>0) && $from!='' && $to!='')
                                   {
                                      echo "<th>"."User Name"."</th>";
                                   }
                                 ?>
                                 <th>File Id</th>
                                 <th>File Name</th>
                                 <th>Remark</th>
                                 <th>Uploaded date</th>
                                 <th>Action / Permission</th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
               <!--End Advanced Tables -->
            </div>
            <!-- /. ROW  -->
            <hr />
         </div>
         <!-- /. PAGE INNER  -->
      </div>
      <!-- /. PAGE WRAPPER  -->
   </div>
   <!-- /. WRAPPER  -->
   <!-- BOOTSTRAP SCRIPTS -->
   <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
   <script src="assets/js/bootstrap.min.js"></script>
   <!-- CUSTOM SCRIPTS -->
   <script src="assets/js/custom.js"></script>
   <!-- Include Date Range Picker -->
   
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
   <script>
      $(document).ready(function(){
      var from_date_input=$('input[name="from_date"]'); //our date input has the name "date"
      var to_date_input=$('input[name="to_date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      from_date_input.datepicker({
      format: 'dd-mm-yyyy',
      container: container,
      todayHighlight: true,
      autoclose: true,
      })
      to_date_input.datepicker({
      format: 'dd-mm-yyyy',
      container: container,
      todayHighlight: true,
      autoclose: true,
      })
      })
   </script>
   <!-- chosen script -->
   <script src="assets/js/chosen.jquery.js" type="text/javascript"></script>
   <script src="assets/js/init.js" type="text/javascript" charset="utf-8"></script>
   <!-- chosen script -->

   
   <!-- DATA TABLE SCRIPTS -->
   <!-- TABLE STYLES-->
   <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" />
   
   <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
   <script>
      $(document).ready(function()
      {
        $('#example').DataTable();
      });
   </script>
   <!-- DATA TABLE SCRIPTS -->

  <script>
    $(document).ready(function()
    {
        $('[data-toggle="tooltip"]').tooltip();   
    });
  </script>


  <!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css"/>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.example-getting-started').multiselect({
                includeSelectAllOption: true,
                onChange: function(option, checked) 
                {
                    // alert('Not triggered when clicking the select all!');
                },
            });
        });
    </script>
</body>
</html>