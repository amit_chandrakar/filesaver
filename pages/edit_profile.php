<?php
    session_start();
    
    if (!isset($_SESSION['name']))
    {
        header('Location: ./index.php');
    }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
      <link rel="icon" href="assets/images/icon.JPG">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
   
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
  <div id="wrapper">
    <?php include('includes/menu.php'); ?>
      
      <div id="page-wrapper" >
          <div id="page-inner">
              <div>
                  <h2 class="text-center">Update User Information</h2><br>
              </div>
              <?php 
                  $id = $_SESSION['id'];
                  require("../config.php");
                  $sql = "SELECT * FROM user where userid = '$id' ";
                  $result = mysql_query($sql,$conn);
                  $row = mysql_fetch_assoc($result);
              ?>
            <form action="update_profile.php" method="POST" name="RegForm" onsubmit="return validate()">
              <div class="row">
            <div class="col-md-3 text-left"><label>Full Name</label></div>
            <div class="col-md-3 text-left">
              <div class="form-group">
                <input type="text" class="form-control disabled" readonly name="ufullname" value="<?php echo $row['ufullname'];?>" autocomplete="off">
              </div>
            </div>

            <div class="col-md-3 text-left"><label>Contact <span style="color: red">*</span></label></div>
            <div class="col-md-3 text-left">
              <div class="form-group">
                <input type="text" class="form-control" name="ucontact" value="<?php echo $row['ucontact'];?>" required autocomplete="off">
              </div>
            </div>

            <div class="col-md-3 text-left"><label>User name <span style="color: red">*</span></label></div>
            <div class="col-md-3 text-left">
              <div class="form-group">
                <input type="text" class="form-control" name="uname" value="<?php echo $row['uname'];?>" required autocomplete="off">
              </div>
            </div>

             <div class="col-md-3 text-left"><label>Email <span style="color: red">*</span></label></div>
            <div class="col-md-3 text-left">
              <div class="form-group">
                <input type="text" class="form-control" name="uemail" value="<?php echo $row['uemail'];?>" required autocomplete="off">
              </div>
            </div>



            <div class="col-md-3 text-left"><label>Password <span style="color: red">*</span></label></div>
            <div class="col-md-3 text-left">
              <div class="form-group">
                <input type="password" class="form-control" name="upwd" value="<?php echo base64_decode($row['upwd']);?>" required autocomplete="off">
              </div>
            </div>
            </div>

            <div class="row">
              <br>
              <div class="col-lg-offset-3 col-md-3 text-right">
                <div class="form-group">
                  <!-- <input type="submit" name="update" class="btn btn-success col-lg-6"> -->
                  <button type="submit" name="update" class="btn btn-success col-lg-6"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <a href="profile.php" class="btn btn-danger col-lg-6"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
              </div>
            </div>

            </form>
        </div>    
    </div><!-- /. PAGE INNER  -->
</div>


    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>

         <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>

   <script type="text/javascript">
      // Form validation code will come here.
      function validate()
      {
        var uname = document.forms["RegForm"]["uname"]; 
        var upwd = document.forms["RegForm"]["upwd"]; 
        var ucontact = document.forms["RegForm"]["ucontact"]; 
        var uemail = document.forms["RegForm"]["uemail"]; 
        var name = document.forms["RegForm"]["ufullname"];

        if (name.value == "" && uname.value == "" && ucontact.value == "" && uemail.value == "" && upwd.value == "")                                 
        {
          window.alert("Please fill all required field.");
          name.focus();
          return false;
        }
        if (uname.value == "")                                 
        {
          window.alert("Please enter user name.");
          name.focus();
          return false;
        }
        if (name.value == "")                                 
        {
          window.alert("Please enter full name.");
          name.focus();
          return false;
        }
        if (upwd.value == "")                                 
        {
          window.alert("Please enter password.");
          upwd.focus();
          return false;
        }
        if (upwd.value.length <= 4)                                 
        {
          window.alert("password should be greater than 4 digit.");
          upwd.focus();
          return false;
        }
        if (ucontact.value == "")                                 
        {
          window.alert("Please enter contact number.");
          ucontact.focus();
          return false;
        }

        if (ucontact.value.length != 10)                                 
        {
          window.alert("Invalid contact number.");
          ucontact.focus();
          return false;
        }
        if (uemail.value == "")                                 
        {
          window.alert("Please enter email id.");
          uemail.focus();
          return false;
        }

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(uemail.value) == false)                                 
        {
          window.alert("Invalid email id.");
          uemail.focus();
          return false;
        }
      }
</script>
   
</body>
</html>
