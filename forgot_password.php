<!DOCTYPE html>
<html>
<head>
	<title>VNR Seeds Pvt. Ltd</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="icon" href="assets/images/icon.jpg">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style type="text/css">
		body
		{
			background-image: url('assets/images/1.jpg');
			background-size: cover;
		}
		.login-heading
		{
			font-size: 30px;
		}
	</style>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-lg-offset-3 col-lg-6"><br><br><br><br><br><br><br><br><br>
				<div class="well">
					<p class="text-center login-heading"><span style="color: red; font-style: Sylfaen">R</span>eset <span style="color: red">P</span>assword</p>
					<form action="home.php">
					    <div class="input-group">
					      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					      <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Registered Email Id">
					    </div><br>
					    
					   
					    <div class="input-group col-lg-offset-4 col-lg-4">
					      <input id="password" type="submit" class="form-control btn btn-default" name="password">
					    </div><br>
					    <a href="index.php" class="text-center col-md-12">Suddenly Remember Password?</a><br>
					</form>
				</div>
			</div>
		</div>
	</div>


	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>