<?php
    session_start();
    if (isset($_SESSION['name']))
    {
        header('Location: pages/index.php');
    }
    
?>
<!DOCTYPE html>
<html>
<head>
	<title>Filesaver</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="icon" href="assets/images/icon.jpg">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style type="text/css">
		body
		{
			background-image: linear-gradient(to left, #c2abb6, #cba6a7, #cba693, #bcaa83, #a0b081);
		}
		.login-heading
		{
			font-size: 30px;
		}
		@media only screen and (max-width: 600px) 
		{
			#email
			{
				height: 50px;
				width: 100%;
			}
			#password
			{
				height: 50px;
				width: 100%;
			}
			.well
			{
				margin-top: 0px;
			}
		}
	</style>
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-lg-offset-3 col-lg-6">
			<div class="well" style="margin-top: 150px;">
				<p class="text-center login-heading"><span style="color: red;">L</span>ogin</p>
				<?php
                    if (isset($_GET['login']) && $_GET['login'] == "fail")
                    {
                        echo "
                        <div class='alert alert-danger alert-dismissible fade in'>
                        <a href='table.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        <strong>Invalid username or password..!</strong>
                        </div>
                        ";
                    }
                    ?>
				<form action="logic.php" method="POST">
				    <div class="input-group">
				      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
				      <input id="email" type="text" class="form-control" name="username" placeholder="Enter Username" autocomplete="off" autofocus required>
				    </div><br>
				    <div class="input-group">
				      <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
				      <input id="password" type="password" class="form-control" name="password" placeholder="Enter Password" autocomplete="off" required>
				    </div><br>
				    <div class="input-group">
		    			<div class="checkbox">
						  <!-- <label><input type="checkbox" name="remember">Remember me</label> -->
						</div>
				    </div>
				    <div class="input-group col-lg-offset-4 col-lg-4">
				      <input type="submit" class="form-control btn btn-default" name="submit" value="Login">
				    </div><br>
				    <!-- <a href="forgot_password.php" class="text-center col-md-12"> Forgot Password?</a><br> -->
				</form>

			</div>
		</div>
	</div>
</div>


	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>

